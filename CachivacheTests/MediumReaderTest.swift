//
//  MediumArticleReaderTest.swift
//  Cachivache
//
//  Created by Hugo on 3/21/16.
//  Copyright © 2016 Hugo Alonso. All rights reserved.
//

import XCTest
@testable import Cachivache

class MediumReaderTest: LoadMockupDataXCTest {
 
    override func setUp() {
        mockupFile = (name: "MediumArticle", ext: "example")
        super.setUp()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        guard let data = data else {
            XCTAssert(false)
            return
        }
        
        guard let reader = MediumReader(data: data) else {
            XCTAssert(false)
            return
        }
        
        let fullText = reader.extractFullText()
        XCTAssert(fullText.characters.count > 100)
        
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
            MediumReader(data: self.data)?.extractFullText()
        }
    }
    
}
