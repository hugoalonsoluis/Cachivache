//
//  UserPostsReaderTests.swift
//  Cachivache
//
//  Created by Hugo Alonso on 4/14/16.
//  Copyright © 2016 Hugo Alonso. All rights reserved.
//

import XCTest
@testable import Cachivache

class UserPostsReaderTests: LoadMockupDataXCTest {
    override func setUp() {
        mockupFile = (name: "cachivache_posts", ext: "json")
        super.setUp()
    }
   
    func testExtractPosts() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        guard let data = data else {
            XCTAssert(false)
            return
        }
        
        guard let posts = NoRSSInterpreter.extractPosts(data) else {
            XCTAssert(false)
            return
        }
        
        XCTAssert(posts.count > 0)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        guard let data = data else {
            XCTAssert(false)
            return
        }
        
        self.measureBlock {
            // Put the code you want to measure the time of here.
            _ = NoRSSInterpreter.extractPosts(data)
        }
    }
    

}
