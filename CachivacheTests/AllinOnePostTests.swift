//
//  AllinOnePostTests.swift
//  Cachivache
//
//  Created by Hugo Alonso on 4/27/16.
//  Copyright © 2016 Hugo Alonso. All rights reserved.
//

import XCTest
@testable import Cachivache

class AllinOnePostTests: LoadMockupDataXCTest {
    override func setUp() {
        mockupFile = (name: "allinOne", ext: "json")
        super.setUp()
    }
    
    func testExtractPosts() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        guard let data = data else {
            XCTAssert(false)
            return
        }
        
        guard let posts = AllinOnePostReader.extractPosts(data) else {
            XCTAssert(false)
            return
        }
        print(posts)
        
        XCTAssert(posts.count > 0)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        guard let data = data else {
            XCTAssert(false)
            return
        }
        
        self.measureBlock {
            // Put the code you want to measure the time of here.
            _ = AllinOnePostReader.extractPosts(data)
        }
    }
}
