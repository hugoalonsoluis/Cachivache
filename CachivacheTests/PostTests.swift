//
//  PostTests.swift
//  Cachivache
//
//  Created by Hugo on 3/16/16.
//  Copyright © 2016 Hugo Alonso. All rights reserved.
//

import XCTest
@testable import Cachivache

class PostTests: XCTestCase {
    var post : Post!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        super.setUp()
        var dict = Dictionary<String, String>()
        dict["title"] = "Test Title"
        dict["description"] = "Test Description"
        dict["link"] = "link"
        dict["pubDate"] = "Mon, 11 Apr 2016 12:45:00 GMT"
        post = Post(dict: dict)
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testDescript() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        print(post.descript())
        XCTAssert(post.descript() == "Test Title link")
    }
    func testExactTitle() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        print(post.exactTitle())
        XCTAssert(post.exactTitle() == "Test Title")
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
