//
//  LoadMockupDataXCTest.swift
//  Cachivache
//
//  Created by Hugo Alonso on 4/14/16.
//  Copyright © 2016 Hugo Alonso. All rights reserved.
//

import XCTest

class LoadMockupDataXCTest: XCTestCase {
    var data: Data!
    var mockupFile : (name: String, ext: String)!
    
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let bundle = Bundle(for: UserPostsReaderTests.classForCoder())
        
        guard
            let file = mockupFile, !file.name.isEmpty && !file.ext.isEmpty,
            let path = bundle.path(forResource: file.name, ofType: file.ext),
            let data = try? Data(contentsOf: URL(fileURLWithPath: path))
        else {
                XCTAssert(false)
                return
        }
        self.data = data
    }

}
