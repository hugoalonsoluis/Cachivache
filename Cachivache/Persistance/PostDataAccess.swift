//
//  UsuarioData.swift
//  Coris App
//
//  Created by Hugo on 12/10/15.
//  Copyright © 2015 coris. All rights reserved.
//

import Foundation
import RealmSwift

open class PostDataAccess  {
    fileprivate init() {}
    
    ///Realm Accessors
    static func persist(_ post: CachivachePost, update: Bool) {
        updateValues { realm in
            realm.add(post, update: update)
        }
    }
    
    static func updateValues( _ updateActions:@escaping (_ realm:Realm)->()) {
        Main {
            let realm = try! Realm()
            try! realm.write({
                updateActions(realm)
            })
        }
    }
    
    static func findByTitle(_ title: String) -> CachivachePost? {
        // Query using a predicate string
        return try! Realm().objects(CachivachePost.self).filter("title BEGINSWITH '\(title)'").first
    }
    
    static func findByLink(_ link: String) -> CachivachePost? {
        // Query using a predicate string
        return try! Realm().objects(CachivachePost.self).filter("link BEGINSWITH '\(link)'").first
    }
    
    
    //Get all non deleted sorteb by publishDate
    static func getPosts() -> [CachivachePost] {
        return try! Realm().objects(CachivachePost.self).sorted(byProperty: "publishedDate", ascending: false).filter { !$0.deleted }
    }
    static func getDeleted() -> [CachivachePost] {
        return try! Realm().objects(CachivachePost.self).sorted(byProperty: "publishedDate", ascending: false).filter { $0.deleted }
    }
}

