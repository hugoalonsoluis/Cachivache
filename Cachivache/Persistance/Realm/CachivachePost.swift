//
//  CachivachePost.swift
//  Cachivache
//
//  Created by Hugo Alonso on 6/25/16.
//  Copyright © 2016 Hugo Alonso. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

class CachivachePost : Object {
    
    dynamic var title: String = ""
    dynamic var subtitle: String  = ""
    dynamic var emailSnippet: String = ""
    
    dynamic var readTime: Double = 0
    dynamic var publishedDate: Date? = nil
    dynamic var tags: String = ""
    
    dynamic var imageId: String = ""
    dynamic var imageAspectRatio_HdivW: Double = 1
    
    dynamic var favorited: Bool = false
    dynamic var deleted: Bool = false
    dynamic var read: Bool = false
    dynamic var allImagesPrecached: Bool = false
    dynamic var link: String = ""
    
    dynamic var readProgress: Int = 0
    
    var postBodyRaw = List<RawComponents>()
    
    func initRealm() {
        _ = try! Realm()
    }
}

extension CachivachePost {
    override static func ignoredProperties() -> [String] {
        return ["postTags","postBody"]
    }
    
    override static func primaryKey() -> String? {
        return "link"
    }
    override static func indexedProperties() -> [String] {
        return ["link","title", "subtitle", "publishedDate", "tags"]
    }
}
