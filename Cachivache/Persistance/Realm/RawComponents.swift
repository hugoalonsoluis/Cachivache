//
//  File.swift
//  Cachivache
//
//  Created by Hugo Alonso on 6/25/16.
//  Copyright © 2016 Hugo Alonso. All rights reserved.
//

import Foundation
import RealmSwift

class RawComponents : Object {
    dynamic var type = 0
    dynamic var text = ""
    dynamic var timeLength = 0
    dynamic var daysElapsed = 0
    dynamic var link = ""
    
    dynamic var imageId = ""
    dynamic var formattedText = ""
    dynamic var mediaResourceId = ""
    dynamic var fullSize = false
    dynamic var orderedList = false
    dynamic var listItemIndex = 0
    dynamic var aspectRatio: Double = 0
    
    convenience init(type: Int, text: String, fullSize: Bool = true) {
        self.init()
        self.type = type
        self.text = text
        self.fullSize = fullSize
    }
    
    convenience init(type: Int = 7, text: String, ordered: Bool = true, index: Int) {
        self.init()
        self.type = type
        self.text = text
        self.orderedList = ordered
        self.listItemIndex = index
    }
    
    
    convenience init(type: Int, imageId: String, aspectRatio: Double, fullSize: Bool, footNote: String) {
        self.init()
        self.type = type
        self.imageId = imageId
        self.aspectRatio = aspectRatio
        self.fullSize = fullSize
        self.text = footNote
    }
    
    convenience init(type: Int,link: String, imageId: String, formattedText: String, rawText: String, mediaResourceId: String, aspectRatio: Double) {
        self.init()
        self.type = type
        self.link = link
        self.imageId = imageId
        self.formattedText = formattedText
        self.text = rawText
        self.mediaResourceId = mediaResourceId
        self.aspectRatio = aspectRatio
    }
    convenience init(type: Int, timeLength: Int, daysElapsed: Int) {
        self.init()
        self.type = type
        self.timeLength = timeLength
        self.daysElapsed = daysElapsed
    }
}
