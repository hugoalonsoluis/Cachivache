//
//  AddTagsViewController.swift
//  Cachivache
//
//  Created by Hugo Alonso on 7/23/16.
//  Copyright © 2016 Hugo Alonso. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

class AddTagsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var newTag: UITextField!
    @IBOutlet weak var saveTagRequested: UIBarButtonItem!
    @IBOutlet weak var cancelRequested: UIBarButtonItem!
    @IBOutlet weak var addNewTag: UIButton!
  
    var currentPost : CachivachePost!
    
    lazy var currentPostTags: [String] = self.currentPost.postTags
    
    var allTags = Variable<[String]>(PostProvider().getTags())
    
    fileprivate let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        GoogleAnalytics.setScreen("Add Tags")
        setupRx()
    }
    
    func setupRx() {
        
        newTag.rx.text
            .throttle(0.5, scheduler: MainScheduler.instance)
            .distinctUntilChanged({ (s1, s2) -> Bool in
                return s1 == s2
            })
            .filter { textIntroduced in
                if let textIntroduced = textIntroduced, textIntroduced.isEmpty {
                    var tags = PostProvider().getTags()
                    tags.insert(contentsOf: self.currentPostTags.filter { !tags.contains($0) }, at: 0)
                    self.allTags.value = tags
                    
                    return false
                }
                
                self.allTags.value = self.allTags.value.filter { $0.hasPrefix(textIntroduced!) }
                
                if self.allTags.value.contains(textIntroduced!) {
                    self.addNewTag.isEnabled = false
                    return false
                }
                
                return true
            }
            .bindNext { self.addNewTag.isEnabled = ($0?.characters.count)! > 3 }
            .addDisposableTo(disposeBag)
   
        allTags
            .asDriver()
            .drive(self.tableView.rx.items(cellIdentifier:"TagItem", cellType: UITableViewCell.self)) { (_, tag, cell) in
                cell.textLabel?.text = tag
               
                if self.currentPostTags.contains(tag) {
                    cell.accessoryType = UITableViewCellAccessoryType.checkmark
                }else {
                    cell.accessoryType = UITableViewCellAccessoryType.none
                }
            }
            .addDisposableTo(disposeBag)

        
        tableView.rx.willDisplayCell
            .asDriver()
            .drive(onNext: { (willDisplayCellEvent) in
                let cell = willDisplayCellEvent.cell
                let index = willDisplayCellEvent.indexPath.row
                
                let tag = self.allTags.value[index]
                
                if self.currentPostTags.contains(tag) {
                    cell.accessoryType = UITableViewCellAccessoryType.checkmark
                }else {
                    cell.accessoryType = UITableViewCellAccessoryType.none
                }

            }, onCompleted: nil, onDisposed: nil)
            .addDisposableTo(disposeBag)

        tableView.rx.itemSelected
            .asDriver()
            .drive(onNext: { (index) in
                
                let tag = self.allTags.value[index.row]
                guard let cell = self.tableView.cellForRow(at: index) else  { return }
                
                if let index = self.currentPostTags.index(of: tag) {
                    cell.accessoryType = UITableViewCellAccessoryType.none
                    self.currentPostTags.remove(at: index)
                }else {
                    cell.accessoryType = UITableViewCellAccessoryType.checkmark
                    self.currentPostTags.append(tag)
                }
            }, onCompleted: nil, onDisposed: nil)
            .addDisposableTo(disposeBag)
        
        saveTagRequested.rx.tap.asDriver()
            .drive(onNext: { (_) in
                
            self.currentPost.setPostTags(self.currentPostTags)
            self.dismissTagsView()
             }, onCompleted: nil, onDisposed: nil)
            .addDisposableTo(disposeBag)
        
        cancelRequested.rx.tap.asDriver()
            .drive(onNext: { (_) in
            self.dismissTagsView()
             }, onCompleted: nil, onDisposed: nil)
            .addDisposableTo(disposeBag)
       
       addNewTag.rx.tap
            .asDriver()
            .drive(onNext: { (index) in
                
                guard let tag = self.newTag.text else { return }
                var composedTag = tag.replacingOccurrences(of: " ", with: "_")
                if composedTag.characters.last == "_" {
                    let last = composedTag.index(before: composedTag.endIndex)
                    composedTag.remove(at: last)
                }
                self.currentPostTags.append(composedTag)
                self.allTags.value.insert(composedTag, at: 0)
                self.newTag.text = nil
                self.newTag.resignFirstResponder()
                self.addNewTag.isEnabled = false
                
            }, onCompleted: nil, onDisposed: nil)
        .addDisposableTo(disposeBag)

        self.addNewTag.isEnabled = false
    }
    
    
    func dismissTagsView() {
        dismiss(animated: true, completion: nil)
    }
}
