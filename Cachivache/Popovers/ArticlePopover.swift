//
//  ArticlePopover.swift
//  Cachivache
//
//  Created by Hugo Alonso on 6/24/16.
//  Copyright © 2016 Hugo Alonso. All rights reserved.
//

import UIKit


protocol ArticlePopoverDelegate {
    func increaseFont()
    func decreaseFont()
    func sharePost()
    func deletePost()
}

class ArticlePopover: UITableViewController {
    
    var actionDelegate : ArticlePopoverDelegate?
    
    
    @IBAction func reduceFont(_ sender: AnyObject) {
        actionDelegate?.decreaseFont()
    }
    @IBAction func increaseFont(_ sender: AnyObject) {
        actionDelegate?.increaseFont()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GoogleAnalytics.setScreen("Article Menu")
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        Main {
            self.dismiss(animated: true) {
                switch indexPath.row {
                case 1: self.actionDelegate?.sharePost()
                case 2: self.actionDelegate?.deletePost()
                default : break
                }
                
            }
        }
    }
    
}
