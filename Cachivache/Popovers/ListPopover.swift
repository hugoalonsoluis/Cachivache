//
//  ListPopover.swift
//  Cachivache
//
//  Created by Hugo Alonso on 6/24/16.
//  Copyright © 2016 Hugo Alonso. All rights reserved.
//

import UIKit


protocol ConfigurationPopoverDelegate {
    func showTour()
    func showAbout()
}

class ListPopover: UITableViewController {
    
    var actionDelegate : ConfigurationPopoverDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GoogleAnalytics.setScreen("List Menu")
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        Main {
            self.dismiss(animated: true) {
                switch indexPath.row {
                case 0 : self.actionDelegate?.showAbout()
                case 1: self.actionDelegate?.showTour()
                default : break
                }
                
            }
        }
    }
    
}
