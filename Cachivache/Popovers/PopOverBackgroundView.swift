//
//  PopOverBackgroundView.swift
//  Cachivache
//
//  Created by Hugo Alonso on 6/23/16.
//  Copyright © 2016 Hugo Alonso. All rights reserved.
//

import UIKit

class PopOverBackgroundView: UIPopoverBackgroundView {
    
    
    override var arrowOffset: CGFloat {
        get{
            return self.arrowOffset
        }
        
        set{
        }
    }
    
    override var arrowDirection: UIPopoverArrowDirection {
        get {
            return UIPopoverArrowDirection.down
        }
        set {
        }
    }
    
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        backgroundColor = UIColor.white//(red: 0.19, green: 0.19, blue: 0.19, alpha: 1.0)
        
        let arrowView = UIImageView(image: UIImage(named: "pop_arrow"))
        arrowView.tintColor = UIColor.white
        arrowView.frame = CGRect(x: frame.width/2 - 5, y: frame.height, width: 10, height: 8.0)
        
        self.addSubview(arrowView)
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.cornerRadius = 0
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override class var wantsDefaultContentAppearance : Bool {
        return false
    }
    
    override class func contentViewInsets() -> UIEdgeInsets{
        return UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
    }
    
    override class func arrowHeight() -> CGFloat {
        return 0.0
    }
    
    override class func arrowBase() -> CGFloat{
        return 24.0
    }
}
