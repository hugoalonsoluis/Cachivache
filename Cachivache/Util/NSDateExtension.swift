//
//  NSDateExtension.swift
//  Cachivache
//
//  Created by Hugo Alonso on 4/10/16.
//  Copyright © 2016 Hugo Alonso. All rights reserved.
//

import Foundation


extension Date {
    
    fileprivate static func buildFormatter(_ shortFormat: Bool) -> DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = shortFormat ? "MMM d, yyyy" :
        "EEE, dd MMM yyyy HH:mm:ss ZZZ"
        return formatter
    }
    
    //NSString to NSDate
    public init(dateString:String, shortFormat : Bool = false) {
        let dateObj = Date.buildFormatter(shortFormat).date(from: dateString)
        if let date = dateObj {
            self.init(timeInterval:0, since: date)
            return
        }
        self.init(timeInterval:0, since: Date())
    }
    
    func toString(_ shortFormat: Bool = false) -> String {
        return Date.buildFormatter(shortFormat).string(from: self)
    }

    public func isAfter(_ otherDate: Date) -> Bool {
        let result  = self.compare(otherDate)
        switch result {
        case ComparisonResult.orderedDescending:
            return true
        default:
            return false
        }
    }
    
    public func minutesInDate() -> Int {
        let theCalendar = Calendar.current
        let min = (theCalendar as NSCalendar).component(NSCalendar.Unit.minute, from: self)
        return min
    }
    
    func daysSinceDate() -> Int{
        return (Calendar.current as NSCalendar).components(NSCalendar.Unit.calendar.symmetricDifference(NSCalendar.Unit.day), from: self, to: Date(), options: NSCalendar.Options.wrapComponents).day!
    }
}
