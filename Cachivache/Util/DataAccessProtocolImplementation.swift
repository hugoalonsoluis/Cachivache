//
//  DataStorageManager.swift
//  Cachivache
//
//  Created by Hugo Alonso on 4/15/16.
//  Copyright © 2016 Hugo Alonso. All rights reserved.
//

import Foundation

/**
 *  The values to be persisted in NSUserDefaults
 *  
 *  Add them here and then in the protocol implementation below
 */
protocol DataAccessProtocol {
    var shouldShowWalktroughModal: Bool { get set }
    var preferedPostFontSizeDiferential: Int { get set }
}
// MARK: - DataAccessProtocol implementation inside DataStorageManager
extension DataStorageManager : DataAccessProtocol {
    
    // MARK: - Locators (keys of the stored values)
    fileprivate var lastVersionWalktroughHasBeingOpened : String { return "lastVersionWalktroughHasBeingOpened" }
    fileprivate var preferedPostFontSizeLocator : String { return "preferedPostFontSize" }
  
    // MARK: - Accessors
    var shouldShowWalktroughModal: Bool {
        get {
            guard let lastVersion = getString(lastVersionWalktroughHasBeingOpened), lastVersion == AppVersion.getFullAppVersion() else {
                return true
            }
            return false
        }
        set {
            let currentVersion : String? = newValue ? "" : AppVersion.getFullAppVersion()
            saveValue(lastVersionWalktroughHasBeingOpened, value: currentVersion as AnyObject?)
        }
    }
    
    var preferedPostFontSizeDiferential: Int {
        get {
            guard let fontSize = getInt(preferedPostFontSizeLocator) else {
                return 0
            }
            return fontSize
        }
        set {
            saveValue(preferedPostFontSizeLocator, value: newValue as AnyObject?)
        }
    }

}
