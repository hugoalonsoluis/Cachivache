//
//  DataManager.swift
//  MiniRev
//
//  Created by Hugo on 7/21/15.
//  Copyright (c) 2015 Hugo Alonso. All rights reserved.
//
import Foundation
/// Handler for writing/reading from standard NSUserDefauls / plist files
open class GeneralDataManager {
    //MARK: vars & Singleton instance
    fileprivate var prefs: UserDefaults
    fileprivate var myDict: NSDictionary?
    
    open static let instance = GeneralDataManager ()
        //MARK: Private Init
    fileprivate convenience init() {
        self.init(userDefaults: UserDefaults.standard)
    }
    fileprivate init(userDefaults: UserDefaults) {
        self.prefs = userDefaults
    }
    //MARK: Loading of Plist Dictionaries
    open func defaultDataIsLoaded() -> Bool {
        return myDict != nil
    }
    
    open func loadPlistDictionary(_ configFileName: String, bundle: Bundle?) -> GeneralDataManager {
        // Read from the Configuration plist the data to make the state of the object valid.
        if myDict != nil {return self}
        myDict = loadDictionary(configFileName,bundle: bundle)
        return self
    }
    
    fileprivate func loadDictionary(_ configFileName: String, bundle: Bundle?) -> NSDictionary? {
        // Read from the Configuration plist the data to make the state of the object valid.
        if bundle == nil {
            print("bundle not found")
            return nil
        }
        if let path = bundle!.path(forResource: configFileName, ofType: "plist") {
            myDict = NSDictionary(contentsOfFile: path)
        }else {
            myDict = nil
        }
        return myDict
    }
    
    //Extract Data from Plist Dictionaries
    open func getDetailsFromDict(_ locator: String) -> String? {
        if let myDict = myDict {
            return getDetailsFromSpecificDict(locator, dictionary: myDict)
        }
        return nil
    }
    
    open func getArrayDetailsFromDict(_ locator: String) -> Array<String>? {
        if let myDict = myDict {
            return getArrayDetailsFromSpecificDict(locator, dictionary: myDict)
        }
        return nil
    }
    
    open func getDetailsFromSpecificDict(_ locator: String, dictionary: NSDictionary) -> String? {
        if let data = dictionary[locator] as? String {
                return data
            }
        return nil
    }
    
    open func getArrayDetailsFromSpecificDict(_ locator: String, dictionary: NSDictionary) -> Array<String>? {
        if let data = dictionary[locator] as? Array<String> {
            return data
        }
        return nil
    }

    
    //MARK: Functions to Handle Store and Retrieve of data from NSUserDefaults
    ///function to save data to NSUserDefaults
    open func saveData(_ locator: String, object: AnyObject){
        let objectEncoded = NSKeyedArchiver.archivedData(withRootObject: object)
        prefs.set(objectEncoded, forKey: locator)
        prefs.synchronize()
    }
    
    ///function to remove data from NSUserDefaults
    open func removeData(_ locator: String){
        prefs.removeObject(forKey: locator)
        prefs.synchronize()
    }
    
    ///function to load data from NSUserDefaults
    open func loadStringData(_ locator: String) -> String? {
        let data: AnyObject? = loadAnyData(locator)
        
        if let _: AnyObject = data {
            let object = data as? String
            return object
        }
        return nil
    }
    
    ///function to load data from NSUserDefaults
    open func loadStringArrayData(_ locator: String) -> Array<String>? {
        let data: AnyObject? = loadAnyData(locator)
        
        if let _: AnyObject = data {
            let object = data as? Array<String>
            return object
        }
        return nil
    }
    
    ///function to load data from NSUserDefaults
    open func loadStringArrayDataConfig(_ locator: String) -> Array<String>? {
        let data: AnyObject? = loadAnyData(locator)
        
        if let _: AnyObject = data {
            let object = data as? Array<String>
            return object
        }
        return nil
    }

    /**
    General function to load any type of data from an NSUserDefaults
    
    - parameter locator: the locator for desired to load data
    
    - returns: the data loaded
    */
    fileprivate func loadAnyData(_ locator: String) -> AnyObject?{
        let data: AnyObject? = prefs.object(forKey: locator) as AnyObject?
        
        if let encodedData: AnyObject = data {
            let object: AnyObject? = NSKeyedUnarchiver.unarchiveObject(with: encodedData as! Data) as AnyObject?
            return object
        }
        return nil
    }
    
}
