//
//  MailHandler.swift
//  MiniRev
//
//  Created by Hugo on 11/25/15.
//  Copyright © 2015 Hugo Alonso. All rights reserved.
//

import Foundation
import MessageUI

struct MailHandler {
    let receipts : [String]
    let subject: String
    let messageBody: String
    
    
    func sendMail(_ parentViewController: MFMailComposeViewControllerDelegate, wellDoneHandler: (()->Void)? = nil, canceledHandler: (()->Void)? = nil) {
        let mailComposeViewController = configuredMailComposeViewController(parentViewController)
        if let parentViewController = parentViewController as? UIViewController {
            if MFMailComposeViewController.canSendMail() {
                parentViewController.present(mailComposeViewController, animated: true, completion: nil)
                wellDoneHandler?()
            } else {
                showSendMailErrorAlert(parentViewController)
                canceledHandler?()
            }
            
        }
    }
    
    fileprivate func showSendMailErrorAlert(_ parentViewController: UIViewController) {
        let message = NSLocalizedString("sendMailError", comment: "Error Sending Email")
        let title = NSLocalizedString("sendMailErrorTitle", comment: "Error Sending Email")
        let sendMailErrorAlert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        sendMailErrorAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        parentViewController.present(sendMailErrorAlert, animated: true, completion: nil)
    }
    
    fileprivate func configuredMailComposeViewController(_ parentViewController: MFMailComposeViewControllerDelegate) -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = parentViewController // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients(receipts)
        mailComposerVC.setSubject(subject)
        mailComposerVC.setMessageBody(messageBody, isHTML: false)
        
        return mailComposerVC
    }
}


extension UIViewController : MFMailComposeViewControllerDelegate {
    // MARK: MFMailComposeViewControllerDelegate Method
    public func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}
