//
//  SocialNetworkLinker.swift
//  MiniRev
//
//  Created by Hugo on 11/24/15.
//  Copyright © 2015 Hugo Alonso. All rights reserved.
//

import Foundation
import UIKit

struct NetworkUrl {
    let scheme: String?
    let page: String
    
    func openPage() {
        var url = URL(string: page)
        
        if let scheme = scheme, let schemeUrl = URL(string: scheme), UIApplication.shared.canOpenURL(schemeUrl) {
                url = schemeUrl
            }
        
        if let url = url {
            UIApplication.shared.openURL(url)
        }
    }
}

enum NetworkLinker {
    case facebook, twitter, instagram, medium
    
    fileprivate func url() -> NetworkUrl {
        var scheme:String?
        var page:String = ""
        
        switch self {
        case .facebook:
            let faceBookPageId: String = ""
            let faceBookPageName = Routes.fb
            
            if faceBookPageId.characters.count > 0 {
                scheme = "fb://profile/\(faceBookPageId)"
            }
            if  faceBookPageName.characters.count > 0{
                page = "https://www.facebook.com/\(faceBookPageName)"
            }
            
        case .twitter:
            let twitterUser = Routes.twiter
            if twitterUser.characters.count > 0 {
                scheme = "twitter://user?screen_name=\(twitterUser)"
                page = "https://twitter.com/\(twitterUser)"
            }
        case .instagram:
            let instagramUser = Routes.instagram
                if instagramUser.characters.count > 0 {
                    scheme = "instagram://user?username=\(instagramUser)"
                    page = "https://www.instagram.com/\(instagramUser)"
                }
            
        case .medium:
            let faqPage = Routes.medium
            if faqPage.characters.count > 0 {
                page = faqPage
            }
        }
        return NetworkUrl(scheme: scheme, page: page)
    }
    
    func openPage() {
        self.url().openPage()
    }
}
