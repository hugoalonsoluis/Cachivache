//
//  MenuHandlers.swift
//  MiniRev
//
//  Created by Hugo on 11/24/15.
//  Copyright © 2015 Hugo Alonso. All rights reserved.
//

import UIKit

enum SubjectOptions : String {
    case WhatIlove = "Lo que me ha gustado", Suggestions = "Sugerencias", BugReport = "Reporte de error", Other = "Otro tema", WhatIDontLike = "Lo que no me gusta"
    
    static let allValues = [WhatIlove, WhatIDontLike, Suggestions, BugReport, Other]
    
    func description() -> String{
        return self.rawValue
    }
}

enum MenuSocialHandlers {
    case review, mediumPage, twitterPage, facebookPage, instagramPage
    
    func goTo() {
        switch self {
        case .facebookPage: NetworkLinker.facebook.openPage()
        case .twitterPage: NetworkLinker.twitter.openPage()
        case .review: return //Still to implement RatingHandler().rate()
        case .mediumPage: NetworkLinker.medium.openPage()
        case .instagramPage: NetworkLinker.instagram.openPage()
        }
    }

    static func sendMail(_ parentViewController: UIViewController) {
        let receipts = [Routes.mail]
        
        requestMailSubject(parentViewController) { subject in
            let mail = MailHandler(receipts: receipts, subject: subject, messageBody: "")
            mail.sendMail(parentViewController)
        }
    }
    
    static fileprivate func requestMailSubject(_ parentViewController: UIViewController, callBack: @escaping (String)->Void) {
        let sendUsEmail = "Comparte tu experiencia"
        let alert = UIAlertController(title: "", message: sendUsEmail, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        for subject in SubjectOptions.allValues{
            //Do something
            
            let bt_action = UIAlertAction(title: subject.description(), style: UIAlertActionStyle.default) { (action) -> Void in
                callBack(subject.description())
            }
            alert.addAction(bt_action)
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil)
        alert.addAction(cancel)
        parentViewController.present(alert, animated: true, completion: nil)
    }
}



