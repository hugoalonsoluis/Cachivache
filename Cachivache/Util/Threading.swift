//
//  Threading.swift
//  MiniRev
//
//  Created by Hugo on 7/25/15.
//  Copyright (c) 2015 Hugo Alonso. All rights reserved.
//
import Foundation

/// Main queue of execution
private var GlobalMainQueue: DispatchQueue {
    return DispatchQueue.main
}
private var GlobalBackgroundQueue: DispatchQueue {
    return DispatchQueue.global(qos: .background)
}

public func executeFunctionInBackground(_ task: @escaping ()->Void, done: (()->Void)? = nil) {
    Background {
        task()
        Main {
            done?()
        }
    }
}

/**
 Allows to call a function after some time
 
 - parameter delayInMilliSeconds: amount of time in milliseconds
 - parameter scheduledTask:       the task to execute
 */
public func callFunctionWithDelay(_ delayInMilliSeconds:Int,scheduledTask: @escaping ()->Void){
    let popTime = DispatchTime.now() + Double(Int64(delayInMilliSeconds * 1000000)) / Double(NSEC_PER_SEC) // Calculates time to wait
    GlobalMainQueue.asyncAfter(deadline: popTime) { // Put on queue the exec of the function after popTime
        
        scheduledTask()
    }
}
/**
 Allows to call a function asynchronously
 
 - parameter asyncFunction: The function to call
 */
public func Main(_ asyncFunction: @escaping () -> Void){
    GlobalMainQueue.async(execute: asyncFunction)
}

func Background(_ function: @escaping (Void) -> Void) {
    GlobalBackgroundQueue.async(execute: function)
}

public func lock(_ obj: AnyObject, blk:() -> ()) {
    objc_sync_enter(obj)
    blk()
    objc_sync_exit(obj)
}
