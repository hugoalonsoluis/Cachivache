//
//  GoogleAnalyticsExtensions.swift
//

import Foundation
import GoogleAnalytics
import Fabric
import Crashlytics

enum EventCategory: String {
    case view_post,
    open_link,
    favorite_post,
    unfavorite_post,
    delete_post,
    modify_post_tags,
    filter_by_tag,
    search_by_keyword,
    social_Action,
    social_Action_share_post,
    app_interaction,
    modify_font_size
}

extension GAI {
    
    // <google analytics constants>
    class var kGADispatchPeriod: TimeInterval { return 30 }
    class var kGATrackUncaughtExceptions: Bool { return true }
    class var kGAIlogLevel: GAILogLevel { return .verbose }
    // </google analytics constants>
    
    class func initializeDefaultTracker(_ kGAPropertyId:String) {
        let sharedInstance = GAI.sharedInstance()
        sharedInstance?.trackUncaughtExceptions = kGATrackUncaughtExceptions
        sharedInstance?.dispatchInterval = kGADispatchPeriod
        sharedInstance?.logger.logLevel = kGAIlogLevel
     // customizing the defualt tracker
        let tracker = sharedInstance?.tracker(withTrackingId: kGAPropertyId)
        // uncomment the lines below for sending to Google Analytics the current version of the app in every hit
        let version = Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as! String
        tracker?.set(kGAIAppVersion, value: version)
     //   tracker.set(kGAIView, value: "128809816")
        // uncomment the line below for limiting the number of hits sent to Google Analytics to a 50 percent
        //tracker.set(kGAISampleRate, value: "50.0")
        tracker?.allowIDFACollection = true
    }
    
    class func sendEvent(_ category: String, action: String, label: String? = nil, value: Int? = nil) {
        let tracker:GAITracker = GAI.sharedInstance().defaultTracker
        let value = NSNumber(value: value ?? 0)
        let build = GAIDictionaryBuilder.createEvent(withCategory: category, action: action, label: label ?? "", value: value).build() as [NSObject : AnyObject]
        tracker.send(build)
    }
    
    class func setScreen(_ screenName: String) {
        let tracker:GAITracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: screenName)
        tracker.send(GAIDictionaryBuilder.createScreenView().build() as [NSObject : AnyObject])
        GAIDictionaryBuilder.createScreenView().build()
    }
}


class GoogleAnalytics {
    class func initializeDefaultTracker() {
        GAI.initializeDefaultTracker("UA-76018491-1")
    }
    
    class func sendEvent(_ category: EventCategory, action: String) {
        GAI.sendEvent("user_action", action: category.rawValue, label: action, value: nil)
        
        switch category {
        case .view_post:
            Answers.logContentView(withName: action,
                                           contentType: "Post",
                                           contentId: nil,
                                           customAttributes: [:])
        break
        default:
            break
        }
       
    }
    
    class func setScreen(_ screenName: String) {
        GAI.setScreen(screenName)
        
    }
    fileprivate init() {}
}
