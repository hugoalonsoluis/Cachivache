//
//  DataStorageManager.swift
//  Cachivache
//
//  Created by Hugo Alonso on 4/15/16.
//  Copyright © 2016 Hugo Alonso. All rights reserved.
//

import Foundation

/// NSUserDefaults manager
final class DataStorageManager: NSObject {
    
    let defaults : UserDefaults!
    /// The singleton reference.
    static let sharedInstance : DataStorageManager = {
        return DataStorageManager()
    }()
    
    override init() {
        defaults = UserDefaults.standard
    }
    /**
     Retrieves as a Bool the value inside the key
     
     - parameter identifier: the key of an stored bool value
     
     - returns: the stored bool value
     */
    func isTrue(_ identifier: String) -> Bool {
        guard let value = defaults.object(forKey: identifier) as? Bool else {
            return false
        }
        return value
    }
    /**
     Retrieves as an String the value inside the key
     
     - parameter identifier: the key of an stored string value
     
     - returns: the stored string value
     */
    func getString(_ identifier: String) -> String? {
        guard let value = defaults.object(forKey: identifier) as? String else {
            return nil
        }
        return value
    }
    /**
     Retrieves as a Int the value inside the key
     
     - parameter identifier: the key of an stored Int value
     
     - returns: the stored Int value
     */
    func getInt(_ identifier: String) -> Int? {
        guard let value = defaults.object(forKey: identifier) as? Int else {
            return nil
        }
        return value
    }
    /**
     Saves any value inside the provider locator
     
     - parameter identifier: the key for storing the value
     - parameter value:      value to store
     */
    func saveValue(_ identifier: String, value: AnyObject?) {
        defaults.set(value, forKey: identifier)
        defaults.synchronize()
    }
}
