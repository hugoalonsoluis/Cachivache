//
//  ErrorHandler.swift
//  Cachivache
//
//  Created by Hugo Alonso on 6/10/16.
//  Copyright © 2016 Hugo Alonso. All rights reserved.
//

import Foundation

class ErrorHandler {
    
    static func reportNewError(_ error: RequestError) {
       print(error.printableErrorDescription())
    }
}
