//
//  AppVersion.swift
//  Cachivache
//
//  Created by Hugo Alonso on 4/15/16.
//  Copyright © 2016 Hugo Alonso. All rights reserved.
//

import Foundation

struct AppVersion {
    /**
     Asks main bundle about current version build
     
     - returns: build number for current version _._.X
     */
    static func getBuildVersion()->String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
    }
    
    /**
     Asks main bundle about current version
     
     - returns: version number X.X
     */
    static func getShortAppVersion()->String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
    }
    
    /**
     Asks main bundle about version and current build of this app
     
     - returns: version number X.X.X
     */
    static func getFullAppVersion()->String {
        return "\(getShortAppVersion()).\(getBuildVersion())"
    }
    
}
