//
//  Collection.swift
//  Cachivache
//
//  Created by Hugo Alonso on 4/15/16.
//  Copyright © 2016 Hugo Alonso. All rights reserved.
//

import Foundation

class Collection {
    static var instance : Collection?
    var id : String? = ""
    var name : String? = ""
    var tags : [String]? = []
    var description : String?  = ""
    var shortDescription : String? = ""
    var image : String? = ""
    var imageW : Int? = 0
    var imageH : Int? = 0
    
    var postCount: Int?  = 0
    var followerCount: Int?  = 0
    
    var twitterUsername: String?  = ""
    var facebookPageName: String?  = ""
    var publicEmail: String?  = ""
    var domain: String? = ""
    var colorPalette : [(color:String, point: Int)]? = []
}

/*
func == (lhs: Collection, rhs: Collection) {
    return
    (lhs.id == rhs.id &&
    lhs.name == rhs.name &&
    lhs.tags == rhs.tags &&
    lhs.description == rhs.description &&
    lhs.shortDescription == rhs.shortDescription &&
    lhs.image == rhs.image &&
    lhs.imageW == rhs.imageW &&
    lhs.imageH == rhs.imageH &&
    lhs.postCount == rhs.postCount &&
    lhs.followerCount == rhs.followerCount &&
    lhs.twitterUsername == rhs.twitterUsername &&
    lhs.facebookPageName == rhs.facebookPageName &&
    lhs.publicEmail == rhs.publicEmail &&
    lhs.domain == rhs.domain &&
    lhs.colorPalette.description == rhs.colorPalette.description)
    
}
    */