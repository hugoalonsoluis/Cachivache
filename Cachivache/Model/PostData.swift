//
//  PostData.swift
//  Cachivache
//
//  Created by Hugo Alonso on 6/25/16.
//  Copyright © 2016 Hugo Alonso. All rights reserved.
//

import Foundation
import RealmSwift

extension CachivachePost {
    
    var postTags : [String] {
        return tags.components(separatedBy: ",").filter { !$0.isEmpty }
    }
    
    func setPostTags(_ newValue:[String]) {
        PostDataAccess.updateValues { (_) in
            self.tags = newValue.joined(separator: ",")
        }
    }
    
    func setPostBody(_ newValue:[Components]) {
        let components = newValue.map  { Components.getRawComponentFrom($0) }
        postBodyRaw.replaceSubrange(0..<postBodyRaw.count, with: components)
    }
    
    var postBody : [Components] {
        return postBodyRaw.map { Components.buildComponentFrom(rawRepresentation: $0) }
    }
    
    
    
    func getReadingTime() -> Int {
        return Int(ceil(readTime))
    }
    
    func getDaysSincePublished() -> Int {
        guard let publishedDate = publishedDate else {
            return 0
        }
        return publishedDate.daysSinceDate()
    }
    func reportOpened() {
        if !self.read {
            PostDataAccess.updateValues { (_) in
                self.read = true
            }
        }
    }
    
    func reportPosition(_ paragraph: Int) {
           PostDataAccess.updateValues { (_) in
                self.readProgress = paragraph
            }
    }
    
    func undelete() {
        if self.deleted {
            PostDataAccess.updateValues { (_) in
                self.deleted = false
            }
        }
    }
    
    
    func reportDeleted() {
        if !self.deleted {
            PostDataAccess.updateValues { (_) in
                self.deleted = true
                self.publishedDate = nil
                self.emailSnippet = ""
                self.readTime = 0
                self.subtitle = ""
                
                self.setPostBody([])
                self.setPostTags([])
            }
        }
    }
}
