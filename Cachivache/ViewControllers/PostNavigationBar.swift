//
//  PostNavigationBar.swift
//  Cachivache
//
//  Created by Hugo Alonso on 5/5/16.
//  Copyright © 2016 Hugo Alonso. All rights reserved.
//

import Foundation
import UIKit

class PostNavigationBar: UINavigationBar {
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        return CGSize(width: size.width, height: 0)
    }
    
    override init(frame: CGRect) {
        var newFrame = frame
        newFrame.size = CGSize(width: frame.width, height: 0)
        super.init(frame: newFrame)

        backgroundColor = UIColor.white
        tintColor = UIColor.white
        barTintColor = UIColor.white
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
        //sizeThatFits(CGSize(width: UIScreen.main.bounds.width, height: 0))
    }
}
