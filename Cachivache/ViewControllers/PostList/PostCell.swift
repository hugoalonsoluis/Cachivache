//
//  PostCell.swift
//  Cachivache
//
//  Created by Hugo Alonso on 4/10/16.
//  Copyright © 2016 Hugo Alonso. All rights reserved.
//

import Foundation
import UIKit

class PostCell : UITableViewCell {
    
    @IBOutlet weak var previewImage: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    
    @IBOutlet weak var tags: UILabel!
    @IBOutlet weak var favoritedBadge: UIView!
    @IBOutlet weak var readBadge: UIView!
    
    var postPreview : FeedNetworkService.PreviewPost?
    
    func updateContent(_ post: FeedNetworkService.PreviewPost) {
        self.postPreview = post
        
        title?.text = post.title
        subtitle?.text = post.snippet
        
        //                if let imageData = object.imageData {
        //                    cell.previewImage?.image = UIImage(data: imageData)
        //                }else {
        //                    cell.previewImage?.image = UIImage(named: "6")
        //                }
        //  cell.nameLabel.sizeToFit()
        //  cell.bannerImage.image = nil
        previewImage.image = nil
        setEditing(false, animated: false)
        
        tags.text = post.tags.joined(separator: "     ")
        tags.sizeToFit()
        
        readBadge.isHidden = true
        favoritedBadge.isHidden = true
        
        if !post.new {
            if let title = post.title,
                let fullPost = PostDataAccess.findByTitle(title) {
                
                favoritedBadge.isHidden = !fullPost.favorited
                readBadge.isHidden = !fullPost.read
               
            }
        }
        let size = ImageSource.ImageSizes.squareLink(originalRatio: 1.12)
        ImageSource.downloadImageAndSetIn(previewImage, imageId: post.imageId!, size: size)
    }
    
//    func buildStringForTags(tags:[String]) -> NSAttributedString {
//        
//        let tagAttributes = [/*NSFontAttributeName: "CourierNew",*/ NSBackgroundColorAttributeName : UIColor.grayColor() , NSForegroundColorAttributeName : UIColor.whiteColor() ]
//        let attributedTags = tags.map { (tag) -> NSAttributedString in
//            return NSAttributedString(string: tag, attributes: tagAttributes)
//        }
//        let finalString = NSMutableAttributedString()
//        let spaceString = NSAttributedString(string: " ")
//       
//        for attTag in attributedTags {
//            finalString.appendAttributedString(attTag)
//            finalString.appendAttributedString(spaceString)
//        }
//        return finalString
//    }
}
