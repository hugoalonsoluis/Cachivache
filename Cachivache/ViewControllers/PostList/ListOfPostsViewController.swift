//
//  ListOfPostsViewController.swift
//  Cachivache
//
//  Created by Hugo Alonso on 6/12/16.
//  Copyright © 2016 Hugo Alonso. All rights reserved.
//
import UIKit
import RxSwift
import RxCocoa
import Result

class ListOfPostsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchButton: UIBarButtonItem!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchBarHeight: NSLayoutConstraint!
    @IBOutlet weak var filterByTag: UIBarButtonItem!
    @IBOutlet weak var filterByFavorite: UIBarButtonItem!
    @IBOutlet weak var showAllUnfiltered: UIBarButtonItem!
    
    let disposeBag = DisposeBag()
    
    fileprivate var keyboardIsVisible: Bool = false
    
    fileprivate var tagsApplied : [String] = [] {
        didSet {
            updateFilterIcon()
        }
    }
    
    
    fileprivate var filterByFavorited : Bool = false {
        didSet {
            updateFavoriteIcon()
        }
    }
    
    func updateFavoriteIcon(){
        filterByFavorite.image = filterByFavorited ? UIImage(named: "favoritos")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal) : UIImage(named: "favoritos")?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        showAllUnfiltered.image = !filterByFavorited ? UIImage(named: "lista")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal) : UIImage(named: "lista")?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
    }
    
    func updateFilterIcon(){
        filterByTag.image = tagsApplied.isEmpty ? UIImage(named: "etiqueta")?.withRenderingMode(UIImageRenderingMode.alwaysTemplate) : UIImage(named: "etiqueta")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
    }
    
    let feedService = FeedService()
    let refreshControl = UIRefreshControl()
    
    let dataSource = Variable<[FeedNetworkService.PreviewPost]>([])
    // let searchDataSource = Variable<[FeedNetworkService.PreviewPost]>([])
    
    var allElementsTemporalList : [FeedNetworkService.PreviewPost]?
    
    /*
    et searchResults = searchBar.rx.text
    .throttle(0.3, scheduler: MainScheduler.instance)
    .distinctUntilChanged()
    .flatMapLatest { query -> Observable<[Repository]> in
    if query.isEmpty {
    return .just([])
    }
    
    return searchGitHub(query)
    .catchErrorJustReturn([])
    }
    .observeOn(MainScheduler.instance)
    
    ... then bind the results to your tableview
    
    searchResults
    .bindTo(tableView.rx.items(cellIdentifier: "Cell")) {
    (index, repository: Repository, cell) in
    cell.textLabel?.text = repository.name
    cell.detailTextLabel?.text = repository.url
    }
    .addDisposableTo(disposeBag)
    */
    
    //TODO: Make this work again
    /*
    var rx_searchString: Observable<String> {
        return searchBar
            .rx.text
            .throttle(0.5, scheduler: MainScheduler.instance)
            
            .distinctUntilChanged({ (s1, s2) -> Bool in
               return s1 == s2
            })
            
            .skip(1)
            
            .filter {
                if $0.isEmpty {
                    //    self.searchDataSource.value.removeAll()
                    //    self.loadingMore = false
                    self.loadDataForPresentation()
                    return false
                } else {
                    let count = $0.characters.count
                    if count < 2 {
                        return false
                    }
                }
                return true
            }
            .doOnNext { (word) in
                GoogleAnalytics.sendEvent(.search_by_keyword, action: word)
                //   self.searchDataSource.value.removeAll()
                //     self.footerView.hidden = false
        }
    }
    */
    var progressViewManager: MediumProgressViewManager?
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let selected =  tableView.indexPathForSelectedRow {
            tableView.reloadRows(at: [selected], with: UITableViewRowAnimation.fade)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GoogleAnalytics.setScreen("Post's List")
        
        tableView.addSubview(refreshControl)
        progressViewManager = MediumProgressViewManager.sharedInstance
        
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableViewAutomaticDimension
        
        setLayoutForKeyboard()
        setupInteractions()
        
        addPullToRefresh()
        appendSubscribers()
        attemptToDownloadNewPosts()
        
        filterByFavorite.image = UIImage(named: "favoritos")?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        filterByTag.image = UIImage(named: "etiqueta")?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
    }
    
    func addPullToRefresh() {
        searchBar.isHidden = true
        searchBarHeight.constant = 0
        GoogleAnalytics.sendEvent(.app_interaction, action: "Pull To Refresh")
        
        // When refresh control emits .ValueChanged, start fetching data
        refreshControl.rx.controlEvent(.valueChanged)
            .bindNext {
                self.downloadPosts()
            }.addDisposableTo(disposeBag)
    }
    
    
    func attemptToDownloadNewPosts() {
        searchBar.isHidden = true
        searchBarHeight.constant = 0
        
        if let progressViewManager = progressViewManager {
            progressViewManager.show(navigationController?.view)
            navigationController?.view.bringSubview(toFront: progressViewManager.progressView!)
        }
        appendSubscribers()
        self.downloadPosts()
    }
    
    var opened = false
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if opened {
            loadDataForPresentation()
        }else {
            opened = true
        }
    }
    
    func loadDataForPresentation() {
        let loadedPosts : Observable<[FeedNetworkService.PreviewPost]>?
        
        if let searchFilter = searchBar.text, !searchFilter.isEmpty {
            loadedPosts = feedService.searchFor(searchFilter)
        }else {
            loadedPosts = feedService.dbFeed
        }
        
        loadedPosts?
            .flatMapLatest(applyFilters)
            .bindTo(dataSource)
            .addDisposableTo(disposeBag)
    }
    
    func applyFilters(_ posts: [FeedNetworkService.PreviewPost]) -> Observable<[FeedNetworkService.PreviewPost]>{
        var postsToShow = posts
        if self.filterByFavorited {
            postsToShow = postsToShow.filter { $0.favorited  }
        }
        
        if !self.tagsApplied.isEmpty {
            postsToShow = postsToShow.filter  {
                for postTag in $0.tags {
                    if self.tagsApplied.contains(postTag) {
                        return true
                    }
                }
                return false
            }
        }
        return Observable.just(postsToShow)
        
    }
    
    func readerViewController() ->(UIViewController?, PostReadViewController?) {
        let navigationDetails = UIStoryboard(name: "PostDetails", bundle: nil).instantiateInitialViewController() as? UINavigationController
        return (navigationDetails, navigationDetails?.viewControllers.first as? PostReadViewController)
    }
    
    
    func setupInteractions() {
        let navigationController = self.navigationController!
        var readyToPresent = true
        
        tableView.rx.itemSelected
            .asDriver()
            .drive(onNext: { (index) in
                
                guard readyToPresent else { return }
                readyToPresent = false
                let controllers = self.readerViewController()
                guard
                    let selectedPost = (self.tableView.cellForRow(at: index) as! PostCell).postPreview,
                    let detailsController = controllers.1,
                    let navigationDetails = controllers.0
                    else { return }
                detailsController.postPreview = selectedPost
                
                navigationController.present(navigationDetails, animated: true, completion: {
                    readyToPresent = true
                })
                
            }, onCompleted: nil, onDisposed: nil)
            .addDisposableTo(disposeBag)
        
        
        tableView.rx.setDelegate(self)
            .addDisposableTo(disposeBag)
        
        searchBar.rx.searchButtonClicked
            .asDriver()
            .drive(onNext: { (_) in
                self.searchBar.resignFirstResponder()
            }, onCompleted: nil, onDisposed: nil)
            .addDisposableTo(disposeBag)
    }
    
    /**
     Called when the user click on the view (outside the UITextField).
     
     - parameter touches: touches set.
     - parameter event:   The event.
     */
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func appendSubscribers() {
        
        //        searchDataSource
        //            .asObservable()
        //            .bindTo(dataSource)
        //            .addDisposableTo(disposeBag)
        //
        
        handleProgressBar()
        
        feedService.dbFeed
            .bindTo(dataSource)
            .addDisposableTo(disposeBag)
        
        dataSource
            .asObservable()
            .bindTo(tableView.rx.items(cellIdentifier: "PostCell", cellType: PostCell.self)) { index, postPreview, cell in
                cell.updateContent(postPreview)
            }.addDisposableTo(disposeBag)
        /*
        tableView.rx.willDisplayCell
            .asDriver()
            .drive(onNext: { willDisplayCellEvent in
        
                let cell = willDisplayCellEvent.cell as! PostCell
                let index = willDisplayCellEvent.indexPath.row
                
                let post = self.dataSource.value[index]
                
                cell.favoritedBadge.isHidden = !post.favorited
                cell.readBadge.isHidden = !post.new

                
            }, onCompleted: nil, onDispose: nil)
        */
    }
    
    func downloadPosts() {
        feedService
            .downloadNew()
            .asDriver(onErrorJustReturn: nil)
            .drive(onNext: { (_) in
                //self.dataSource.value.append(post!)
                self.refreshControl.endRefreshing()
                self.loadDataForPresentation()
            }, onCompleted: nil, onDisposed: nil)
            .addDisposableTo(disposeBag)
    }
    
    @IBAction func searchButtonTapped(_ sender: AnyObject) {
        let searchBar = self.searchBar
        let dataSource = self.dataSource
        //  let searchDataSource = self.searchDataSource
        let disposeBag = self.disposeBag
        let feedService = self.feedService
        
        searchBar?.isHidden = !(searchBar?.isHidden)!
        self.searchBarHeight.constant = (searchBar?.isHidden)! ? 0 : 44
        UIView.animate(withDuration: 0.35, animations: {
            self.view.layoutIfNeeded()
            //   searchDataSource.value = []
        }, completion:  { _ in
            if (searchBar?.isHidden)! {
                searchBar?.endEditing(true)
                searchBar?.text = ""
                self.loadDataForPresentation()
                
            }else {
                searchBar?.becomeFirstResponder()
                searchBar?.text = ""
                //aqui estaba searchString
                searchBar?.rx.text
                    .flatMapLatest { feedService.searchFor($0!) }
                    .flatMapLatest(self.applyFilters)
                    .bindTo(dataSource)
                    .addDisposableTo(disposeBag)
            }
        })
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if keyboardIsVisible {
            searchBar.resignFirstResponder()
        }
    }
    
    fileprivate func handleProgressBar() {
        NotificationCenter.default
            .rx.notification(Notification.Name(rawValue: "StartDownloading"), object: nil)
            .observeOn(MainScheduler.instance)
            .bindNext { [weak self] notification in
                self?.progressViewManager?.show()
            }
            .addDisposableTo(disposeBag)
        
        NotificationCenter.default
            .rx.notification(Notification.Name(rawValue: "DoneDownloading"), object: nil)
            .observeOn(MainScheduler.instance)
            .bindNext { [weak self] notification in
                self?.progressViewManager?.hide()
                self?.loadDataForPresentation()
            }
            .addDisposableTo(disposeBag)
    }
    
    fileprivate func setLayoutForKeyboard() {
        
        NotificationCenter.default
            .rx.notification(NSNotification.Name.UIKeyboardWillShow, object: nil)
            .observeOn(MainScheduler.instance)
            .bindNext { [weak self] notification in
                
                guard let info = notification.userInfo else { return }
                guard let strongSelf = self else { return }
                guard !strongSelf.keyboardIsVisible else { return }
                
                let keyboardFrame: CGRect = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
                strongSelf.keyboardIsVisible = true
                
                let contentInsets = UIEdgeInsetsMake(0, 0.0, keyboardFrame.height, 0.0);
                strongSelf.tableView.contentInset = contentInsets
                strongSelf.tableView.scrollIndicatorInsets = contentInsets
            }
            .addDisposableTo(disposeBag)
        
        NotificationCenter.default
            .rx.notification(NSNotification.Name.UIKeyboardWillHide, object: nil)
            .observeOn(MainScheduler.instance)
            .bindNext { [weak self] notification in
                guard let strongSelf = self else { return }
                
                strongSelf.keyboardIsVisible = false
                
                let contentInsets = UIEdgeInsets.zero
                strongSelf.tableView.contentInset = contentInsets
                strongSelf.tableView.scrollIndicatorInsets = contentInsets
            }
            .addDisposableTo(disposeBag)
    }
    
    @IBAction func showAll(_ sender: AnyObject) {
        tagsApplied = []
        filterByFavorited = false
        loadDataForPresentation()
        GoogleAnalytics.sendEvent(.app_interaction, action: "Show All Posts")
        
    }
    @IBAction func showFavorited(_ sender: AnyObject) {
        tagsApplied = []
        filterByFavorited = true
        loadDataForPresentation()
        GoogleAnalytics.sendEvent(.app_interaction, action: "Show Favorites")
    }
    
    @IBAction func showTags(_ sender: AnyObject) {
        let tagSelector = UIStoryboard(name: "Tags", bundle: nil).instantiateViewController(withIdentifier: "TagsFilterViewController") as! TagsFilterViewController
        tagSelector.delegate = self
        present(tagSelector, animated: true, completion: nil)
        GoogleAnalytics.sendEvent(.app_interaction, action: "Show Tags")
        
    }
    
    @IBAction func showConfig(_ sender: UIButton) {
        
        let popoverContent = UIStoryboard(name: "Popovers", bundle: nil).instantiateViewController(withIdentifier: "ListOptions") as! ListPopover
        popoverContent.actionDelegate = self
        popoverContent.modalPresentationStyle = UIModalPresentationStyle.popover
        
        let popover = popoverContent.popoverPresentationController
        
        popoverContent.preferredContentSize = CGSize(width: 100, height: 90)
        popover!.delegate = self
        popover!.sourceView = sender.imageView
        popover!.popoverLayoutMargins = UIEdgeInsetsMake(0, 0, 0, 0)
        popover!.sourceRect = CGRect(x: 10,y: -24,width: 0,height: 0)
        popover!.permittedArrowDirections = .down
        popover!.popoverBackgroundViewClass = PopOverBackgroundView.self
        
        self.present(popoverContent, animated: true, completion: nil)
    }
}


extension ListOfPostsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let postPreview = dataSource.value[indexPath.row]
        
        let delete = UITableViewRowAction(style: .normal, title: "Eliminar", handler: actionForSwipeList)
        delete.accessibilityHint = "eliminar"
        delete.backgroundColor = UIColor.red.withAlphaComponent(0.6)
        
        let favorite = UITableViewRowAction(style: .normal, title: postPreview.favorited ? "Desmarcar" : "Marcar", handler: actionForSwipeList)
        favorite.accessibilityHint = "favorito"
        favorite.backgroundColor = UIColor.yellow.withAlphaComponent(0.6)
        
        let tagging = UITableViewRowAction(style: .normal, title: "Etiquetar", handler: actionForSwipeList)
        tagging.accessibilityHint = "etiquetar"
        tagging.backgroundColor = UIColor.green.withAlphaComponent(0.6)
        
        return [delete, tagging, favorite]
    }
    
    /**
     Handler for the three-dot button in the swipe left.
     
     - parameter action: The action for row
     - parameter index:  The index for the cell selected
     */
    fileprivate func actionForSwipeList(_ action: UITableViewRowAction, index: IndexPath) {
        
        
        let postPreview = dataSource.value[index.row]
        guard
            let title = postPreview.title,
            let fullPost = PostDataAccess.findByTitle(title)
            else { return }
        
        switch action.accessibilityHint! {
        case "favorito":
            PostDataAccess.updateValues({ _ in
                fullPost.favorited = !fullPost.favorited
                postPreview.favorited = fullPost.favorited
                
                CATransaction.begin()
                CATransaction.setCompletionBlock({
                    self.tableView.reloadRows(at: [index], with: UITableViewRowAnimation.fade)
                })
                self.tableView.setEditing(false, animated: true)
                
                CATransaction.commit()
                let action : EventCategory = postPreview.favorited ? .favorite_post : .unfavorite_post
                GoogleAnalytics.sendEvent(action, action: postPreview.title!)
                
            })
        case "eliminar" : deletePost(fullPost)
        case "etiquetar": modifyTags(fullPost)
        GoogleAnalytics.sendEvent(.modify_post_tags, action: postPreview.title!)
        default:
            break
        }
        
        tableView.reloadData()
        self.tableView.setEditing(false, animated: true) // return the tableview to not editing mode
        
    }
    
    func modifyTags(_ fullPost: CachivachePost) {
        let tagEditor = UIStoryboard(name: "Tags", bundle: nil).instantiateViewController(withIdentifier: "AddTagsViewController") as! AddTagsViewController
        tagEditor.currentPost = fullPost
        present(tagEditor, animated: true, completion: nil)
        
    }
    
    func deletePost(_ fullPost: CachivachePost) {
        //   let alert = UIAlertController(title: "Esta seguro?", message: "Esta seguro de querer eliminar este articulo? Esto accion no se puede deshacer.", preferredStyle: UIAlertControllerStyle.Alert)
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let deleteAction = UIAlertAction(title: "Eliminar Artículo", style: UIAlertActionStyle.destructive) { (_) in
            fullPost.reportDeleted()
            self.loadDataForPresentation()
            GoogleAnalytics.sendEvent(.delete_post, action: fullPost.title)
        }
        let cancel = UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.cancel, handler: nil)
        alert.addAction(cancel)
        alert.addAction(deleteAction)
        
        self.present(alert, animated: true, completion: nil)
    }
}


extension ListOfPostsViewController: ConfigurationPopoverDelegate {
    func showTour() {
        if let tour = UIStoryboard(name: "Walkthrough", bundle: nil).instantiateInitialViewController() {
            navigationController?.present(tour, animated: true, completion: nil)
        }
    }
    func showAbout() {
        if let about = UIStoryboard(name: "About", bundle: nil).instantiateInitialViewController() {
            navigationController?.pushViewController(about, animated: true)
        }
    }
}

extension ListOfPostsViewController: UIPopoverPresentationControllerDelegate {
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func popoverPresentationController(_ popoverPresentationController: UIPopoverPresentationController, willRepositionPopoverTo rect: UnsafeMutablePointer<CGRect>, in view: AutoreleasingUnsafeMutablePointer<UIView>) {
        let x = popoverPresentationController.presentingViewController.view.center
        let newRect = CGRect(x: x.x , y: x.y, width: 0, height: 0)
        rect.initialize(to: newRect)
    }
    
}

extension ListOfPostsViewController: TagFilterDelegate {
    
    func filtersToApply(_ selectedTags:[String]) {
        tagsApplied = selectedTags
        
        if tagsApplied.isEmpty {
            //   filterByTag.badgeValue = 0
        }
        loadDataForPresentation()
        
    }
    
    func appliedTagFilter() -> [String] {
        return tagsApplied
    }
}

extension ListOfPostsViewController : UITextFieldDelegate {
    /**
     Called when 'return' key pressed. return NO to ignore.
     
     - parameter textField: The delegating textField
     
     - returns: true always
     */
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
