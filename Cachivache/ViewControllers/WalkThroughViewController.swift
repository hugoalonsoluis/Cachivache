//
//  WalkThroughViewController.swift
//  foraspace-ios
//
//  Created by Hugo on 5/2/16.
//  Copyright © 2016 Victor Sigler. All rights reserved.
//

import UIKit

/// Walktrough Handler
class WalkThroughViewController: UIViewController, UIScrollViewDelegate{
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var swipeLeftRecognizer: UISwipeGestureRecognizer!
    
    let pageCount = 3
    let borderColor = UIColor.black
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GoogleAnalytics.setScreen("Walkthrough")
        pageControl.currentPage = 0
        pageControl.numberOfPages = self.pageCount
        scrollView.panGestureRecognizer.require(toFail: swipeLeftRecognizer)
        scrollView.backgroundColor = UIColor.white
    }
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let pagesScrollViewSize = scrollView.frame.size
        
        scrollView.contentSize = CGSize(width: pagesScrollViewSize.width * CGFloat(pageCount), height: pagesScrollViewSize.height)
        
        for page in 0..<pageCount {
            
            var frame = scrollView.bounds
            frame.origin.x = frame.size.width * CGFloat(page)
            frame.origin.y = 0.0
            
            let pageName = "walkthrough_page_\(page + 1)" //NSLocalizedString("page\(page + 1)", comment: "Page number")
            let newPageView = UIImageView(image: UIImage(named: "\(pageName).jpg"))
            newPageView.contentMode = UIViewContentMode.scaleAspectFit
            newPageView.frame = frame
            newPageView.clipsToBounds = true
            scrollView.addSubview(newPageView)
        }
        self.view.bringSubview(toFront: pageControl)
    }
    
    @IBAction func changePage(_ sender: AnyObject){
        let page = pageControl.currentPage
        var frame = scrollView.frame
        frame.origin.x = frame.size.width * CGFloat(page)
        frame.origin.y = 0
        scrollView.scrollRectToVisible(frame, animated:true)
    }
    
    func loadVisiblePage() {
        // First, determine which page is currently visible
        let pageWidth = scrollView.frame.size.width
        let page = Int(floor((scrollView.contentOffset.x * 2.0 + pageWidth) / (pageWidth * 2.0)))
        
        // Update the page control
        pageControl.currentPage = page
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if pageControl.currentPage == pageCount - 1 {
            swipeLeftRecognizer.isEnabled = true
        }else {
            swipeLeftRecognizer.isEnabled = false
        }
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
         self.loadVisiblePage()
    }
    
    @IBAction func close() {
        
        if DataStorageManager.sharedInstance.shouldShowWalktroughModal {
            //Hide the walkthrough the next times the app is opened
            DataStorageManager.sharedInstance.shouldShowWalktroughModal = false
           
        
            if let postList = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController() {
                present(postList, animated: true, completion: nil)
            }
        }else {
            dismiss(animated: true, completion: nil)
        }
        
    }
}
