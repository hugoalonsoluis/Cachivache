//
//  SplashViewController.swift
//  foraspace-ios
//
//  Created by Hugo on 5/2/16.
//  Copyright © 2016 Victor Sigler. All rights reserved.
//

import UIKit
import RxSwift


enum InitialNavigation {
    case walkthrough, main
}

class SplashViewController: UIViewController {
    
    @IBOutlet weak var image: UIImageView!
    var continueAnim = false
    let disposebag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Get the launch image regarding the orientation of the device.
        if let image = UIImage(named: self.splashImageForOrientation(UIApplication.shared.statusBarOrientation)) {
            self.image.image = image
        }
        GoogleAnalytics.setScreen("Splash Screen")
        let destination = initialView()
        proceedIntoTheApp(destination)
//        let feedService = FeedService()
//        
//        feedService.allPosts
//            .subscribe()
//            .addDisposableTo(disposebag)
//        
        //        FeedNetworkService.networkPostPreviewFeed.bindNext { (previewPosts) in
        //
        //            }.addDisposableTo(disposebag)
        //
        //        FeedService().newPostCards.bindNext { (previewPosts) in
        //                print(previewPosts.count)
        //        }.addDisposableTo(disposebag)
    }
    
    /**
     The first view the user will interact with
     
     - returns: Next step in navigation (Walkthrough, Main)
     */
    func initialView() -> InitialNavigation {
        guard !DataStorageManager.sharedInstance.shouldShowWalktroughModal else {
            return .walkthrough
        }
        return .main
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        continueAnim = true
        animateCachivache()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        continueAnim = false
    }
    
    func animateCachivache() {
        enlargeCachivache(rotateChachivache)
    }
    
    func enlargeAndExitCachivache(_ done: @escaping ()->()){
        UIView.animate(withDuration: 0.9, delay: 1.5, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.image.transform = self.image.transform.scaledBy(x: 250, y: 250)
            }, completion: { _ in
                done()
        })
    }
    
    func enlargeCachivache(_ done: @escaping ()->()){
        UIView.animate(withDuration: 1.0, delay: 0, options: UIViewAnimationOptions(), animations: {
            self.image.transform = self.image.transform.scaledBy(x: 1.3, y: 1.3)
            }, completion: { _ in
                if self.continueAnim {
                    done()
                }
        })
    }
    
    func rotateChachivache(){
        UIView.animate(withDuration: 1.5, delay: 0, options: UIViewAnimationOptions(), animations: {
            self.image.transform = self.image.transform.rotated(by: 360)
            }, completion: { _ in
                if self.continueAnim {
                    self.rotateChachivache()
                }
        })
    }
    /**
     Opens the first view the user will interact with
     
     - parameter destination: next step in navigation
     */
    func proceedIntoTheApp(_ destination: InitialNavigation) {
        
        let storyboard : UIStoryboard!
        switch destination {
        case .walkthrough:
            storyboard = UIStoryboard(name: "Walkthrough", bundle: nil)
        case .main:
            storyboard = UIStoryboard(name: "Main", bundle: nil)
            
        }
        openInitialViewOf(storyboard)
    }
    
    func openInitialViewOf(_ storyboard: UIStoryboard) {
        let viewController = storyboard.instantiateInitialViewController()!
        
        enlargeAndExitCachivache() {
            Main {
                self.present(viewController, animated: true, completion: {
                    self.continueAnim = false
                })
            }
        }
        
    }
    
    
    /**
     Displays the correct launch image regarding the device orientation.
     
     :param: orientation The device orientation.
     
     :returns: The name of the launch image.
     */
    func splashImageForOrientation(_ orientation: UIInterfaceOrientation) -> String {
        
        var viewSize = self.view.bounds.size
        var viewOrientation = "Portrait"
        
        if UIInterfaceOrientationIsLandscape(orientation) {
            viewSize = CGSize(width: viewSize.height, height: viewSize.width)
            viewOrientation = "Landscape"
        }
        
        
        if let imagesDict = Bundle.main.infoDictionary, let imagesArray = imagesDict["UILaunchImages"] as? NSArray {
            for dict in imagesArray {
                let dictNSDict = dict as! NSDictionary
                let imageSize = CGSizeFromString(dictNSDict["UILaunchImageSize"] as! String)
                if imageSize.equalTo(viewSize) && viewOrientation == (dictNSDict["UILaunchImageOrientation"] as! String) {
                    return dictNSDict["UILaunchImageName"] as! String
                }
            }
        }
        return ""
    }
    
}
