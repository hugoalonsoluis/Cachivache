//
//  TagsFilterViewController.swift
//  Cachivache
//
//  Created by Hugo Alonso on 7/24/16.
//  Copyright © 2016 Hugo Alonso. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol TagFilterDelegate : class {
    func filtersToApply(_ selectedTags:[String])
    func appliedTagFilter() -> [String]
}

class TagsFilterViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var tagsTableHeight: NSLayoutConstraint!
    @IBOutlet weak var topView: UIView!
    
    var allTags = Variable<[String]>(PostProvider().getTags())
    lazy var selectedFilters = [String]()

    fileprivate let disposeBag = DisposeBag()
    
    weak var delegate : TagFilterDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GoogleAnalytics.setScreen("Tag's Filter")
        
        if let filters = delegate?.appliedTagFilter(), !filters.isEmpty {
            selectedFilters = filters
        }
        
        setupRx()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.tagsTableHeight.constant = self.tableView.contentSize.height
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        UIView.animate(withDuration: 0.3, animations: {
            self.topView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        }) 
    }
    
    func setupRx() {
        
        allTags
            .asDriver()
            .drive(self.tableView.rx.items(cellIdentifier: "TagItem", cellType: UITableViewCell.self)) { (_, tag, cell) in
                cell.textLabel?.text = tag
                
                if self.selectedFilters.contains(tag) {
                    cell.accessoryType = UITableViewCellAccessoryType.checkmark
                }else {
                    cell.accessoryType = UITableViewCellAccessoryType.none
                }
            }
            .addDisposableTo(disposeBag)
        
        
        tableView.rx.willDisplayCell
            .asDriver()
            .drive(onNext: { (willDisplayCellEvent) in
              
                let cell = willDisplayCellEvent.cell
                let index = willDisplayCellEvent.indexPath.row
                
                let tag = self.allTags.value[index]
                
                if self.selectedFilters.contains(tag) {
                    cell.accessoryType = UITableViewCellAccessoryType.checkmark
                }else {
                    cell.accessoryType = UITableViewCellAccessoryType.none
                }

            }, onCompleted: nil, onDisposed: nil)
            .addDisposableTo(disposeBag)
        
        tableView.rx.itemSelected
            .asDriver()
            .drive(onNext: { (index) in
               
                let tag = self.allTags.value[index.row]
                guard let cell = self.tableView.cellForRow(at: index) else  { return }
                
                if let index = self.selectedFilters.index(of: tag) {
                    cell.accessoryType = UITableViewCellAccessoryType.none
                    self.selectedFilters.remove(at: index)
                }else {
                    cell.accessoryType = UITableViewCellAccessoryType.checkmark
                    self.selectedFilters.append(tag)
                }
                
            }, onCompleted: nil, onDisposed: nil)
            .addDisposableTo(disposeBag)
        
        filterButton.rx.tap
            .asDriver()
            .drive(onNext: { (_) in
           
                if self.selectedFilters.count == self.allTags.value.count {
                    self.delegate?.filtersToApply([])
                }else {
                    self.delegate?.filtersToApply(self.selectedFilters)
                    GoogleAnalytics.sendEvent(.filter_by_tag, action: self.selectedFilters.description)
                }
                self.dismissTagsView()
            }, onCompleted: nil, onDisposed: nil)
            .addDisposableTo(disposeBag)
    }
    
    @IBAction func cancelFilter(_ sender: AnyObject) {
        dismissTagsView()
    }
    func dismissTagsView() {
        UIView.animate(withDuration: 0.3, animations: {
            self.topView.backgroundColor = UIColor.black.withAlphaComponent(0)
        }, completion: { _ in
            self.dismiss(animated: true, completion: nil)
        })
    }
}
