//
//  ReaderCell.swift
//  Cachivache
//
//  Created by Hugo Alonso on 7/27/16.
//  Copyright © 2016 Hugo Alonso. All rights reserved.
//

import UIKit

class ReaderCell: UITableViewCell {
  
    @IBOutlet weak var textPresenter: UITextView!
    
    var readyToPresentText : NSAttributedString?
    
    func cleanFields() {
        readyToPresentText = nil
        textPresenter.attributedText = nil
        textPresenter.text = nil
    }
    
    func setupCell(_ text: String) {
        prepareText(text)
    }
    
    func setupCellWithString(_ attributed: NSAttributedString) {
        self.textPresenter.attributedText = attributed
    }
    
    func updateUI(){
        guard let readyToPresentText = readyToPresentText else { return }
     //   Main {
            self.textPresenter.attributedText = readyToPresentText
           
     //   }
    }
    
    func getDefaultFontSize() -> Int { fatalError("This must be implemented") }
    func getDefaultFontName() -> String { return "FF Kievit Pro"}
    func getDefaultFontColor() -> String { return "#000000"}
    
    func prepareText(_ text:String){
        readyToPresentText = prepareTextForPresentation(text)
        updateUI()
    }
    
    func prepareTextForPresentation(_ text:String) -> NSAttributedString {
        let htmlText = "<html><head><style type='text/css'>body{font-family:'\(getDefaultFontName())'; color: \(getDefaultFontColor());font-size: \(getDefaultFontSize() + FontSizeHandler.diferential)px;}</style></head><body>\(text)</body></html>"
        
        let text = htmlText.mutableHTMLToShow
        text.addAttribute(NSParagraphStyleAttributeName, value: ParagraphCell.paragraphStyle, range: NSMakeRange(0, text.length))
        return text
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        cleanFields()
    }
    
    //-----
    
    static let paragraphStyle : NSMutableParagraphStyle = {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .justified
        paragraphStyle.firstLineHeadIndent = 8
        paragraphStyle.lineSpacing = 5
        paragraphStyle.paragraphSpacing = 8
        paragraphStyle.hyphenationFactor = 1.0
        return paragraphStyle
    }()
}

extension String {
    
    var mutableHTMLToShow: NSMutableAttributedString {
        
        let encodedData = self.data(using: String.Encoding.utf8)!
        
        let attributedOptions : [String: AnyObject] = [
            NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType as AnyObject,
            NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue as AnyObject
        ]
        
        let attString = try! NSMutableAttributedString(data: encodedData, options: attributedOptions, documentAttributes: nil)
        
        return attString
    }
    
}
