//
//  PostReadViewController.swift
//  Cachivache
//
//  Created by Hugo Alonso on 6/25/16.
//  Copyright © 2016 Hugo Alonso. All rights reserved.
//

import UIKit

protocol CacheImageProvider {
    var  images : [String: UIImage]? { get set}
    func getImageForId(_ imageID: String) -> UIImage?
    func preloadImage(_ imageId:String, aspectRatio: Double)
    func preloadImages(_ postBody: [Components])
}

struct FontSizeHandler {
    static var diferential : Int {
        get {
            return DataStorageManager().preferedPostFontSizeDiferential
        }
        set {
            DataStorageManager().preferedPostFontSizeDiferential = newValue
        }
    }
}

class PostReadViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var favoriteButton: UIBarButtonItem!
    
    var images : [String: UIImage]?
    var texts:[IndexPath: NSAttributedString] = [:]
    var cells:[IndexPath: UITableViewCell] = [:]
    
    var postPreview : FeedNetworkService.PreviewPost? {
        didSet {
            if postPreview?.link != oldValue?.link {
                tableView?.contentOffset = CGPoint(x: 0, y: 0)
                executeFunctionInBackground({
                    self.loadArticle()
                })
            }
        }
    }
    
    var detailItem: CachivachePost? {
        didSet {
            guard let detailItem = detailItem, detailItem != oldValue else { return }
            texts = [:]
            postBody = detailItem.postBody
            preloadImages(postBody)
            
            if tableView != nil {
                if let oldValue = oldValue {
                    let oldItems = oldValue.postBody.count
                    var idxs = [IndexPath]()
                    for var i in 0..<oldItems {
                        idxs.append(IndexPath(row: i, section: 0))
                        i = i + 1
                    }
                    
                    tableView.deleteRows(at: idxs, with: UITableViewRowAnimation.none)
                }
                /*  for i in 0..<self.postBody.count {
                 let indexPath = IndexPath(row: i, section: 0)
                 if let cell = self.getCellNoIndexPath(self.tableView, indexPath: indexPath) as? ReaderCell {
                 
                 self.texts[indexPath] = cell.textPresenter.attributedText
                 }
                 }*/
                self.configureView()
            }
        }
    }
    
    
    var postBody = [Components]()
    var updatePostPosition = false
    func configureView() {
        Main {
            self.tableView.contentOffset = CGPoint(x: 0,y: 0)
            self.tableView.reloadData()
            self.updateFavoriteIcon()
            self.updatePostPosition = true
        }
    }
    
    func updateFavoriteIcon(){
        favoriteButton.image = postPreview!.favorited ? UIImage(named: "favoritos")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal) : UIImage(named: "favoritos")?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
    }
    
    func moveToLastPosition(){
        if let index = detailItem?.readProgress {
            tableView.scrollToRow(at: IndexPath(item: index, section: 0), at: UITableViewScrollPosition.middle, animated: false)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIApplication.shared.isIdleTimerDisabled = true
        GoogleAnalytics.setScreen("Post's Content")
        GoogleAnalytics.sendEvent(.view_post, action: postPreview!.title!)
        if updatePostPosition {
            updatePostPosition = false
            self.moveToLastPosition()
            UIView.animate(withDuration: 0.15, animations: {
                self.tableView.alpha = 1
            })
        }else {
            tableView.alpha = 1
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.alpha = 0
    }
    
    var readingCompleteness: CGFloat {
        get {
            let completeness = tableView.contentOffset.y / tableView.contentSize.height
            print(completeness)
            return completeness
        }
        set {
            let y = newValue * tableView.contentSize.height
            print("Move to position =\(y) of \(tableView.contentSize.height)")
            tableView.setContentOffset(CGPoint(x: 0, y: y), animated: false)
        }
    }
    
    override var prefersStatusBarHidden : Bool {
        if self.navigationController?.isNavigationBarHidden == true {
            return true
        } else {
            return false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        //    tableView.rxcell
        //        tableView.estimatedRowHeight = 100
        //        //You can put anything in place of 100.
        //        tableView.rowHeight = UITableViewAutomaticDimension
        //
        if detailItem == nil {
            executeFunctionInBackground({
                self.loadArticle()
            }, done: {
                //  self.configureView()
            })
        }else {
            configureView()
        }
    }
    
    
    
    func loadArticle() {
        guard
            let postPreview = postPreview,
            let link = postPreview.link
            else { return }
        
        Main {
            if let fullPost = PostDataAccess.findByLink(link) {
                self.detailItem = fullPost
                fullPost.reportOpened()
            }
        }
        
        
    }
}


extension PostReadViewController: UITableViewDataSource, UITableViewDelegate {
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        UIApplication.shared.isIdleTimerDisabled = false
        reportReadingPositionChange()
        cells = [:]
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        reportReadingPositionChange()
    }
    
    func reportReadingPositionChange() {
        
        let index = tableView.visibleCells.count / 2
        let cell = tableView.visibleCells[index]
        guard
            let position = tableView.indexPath(for: cell)?.row
            else { return }
        
        print("Save article for \(position)")
        
        
        Main {
            self.detailItem?.reportPosition(position)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return postBody.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return cells[indexPath] ?? getCell(tableView, indexPath: indexPath)
        //return
    }
    
    func getCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        switch postBody[indexPath.row] {
        case let .image(imageId, aspectRatio, _, footNote):
            
            let identifier = "FullSizeImageCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ImageCell
            
            
            /* if let text = texts[indexPath] {
             cell.setupCellWithAttributedString(imageId, footNote: text, aspectRatio: aspectRatio, preloadImage: self.getImageForId(imageId))
             }else {
             */    cell.setupCell(imageId, footNote: footNote, aspectRatio: aspectRatio, preloadImage: self.getImageForId(imageId))
            // }
            return cell
            
        case let .link(link, imageId, formattedText, rawText, _, aspectRatio):
            let identifier = "LinkCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! LinkCell
            
            if let text = texts[indexPath] {
                cell.setupCell(link, imageId: imageId, formattedText: text, aspectRatio: aspectRatio)
            }else {
                cell.setupCell(link, imageId: imageId, formattedText: formattedText, rawText:rawText, aspectRatio: aspectRatio)
                texts[indexPath] = cell.textPresenter.attributedText
            }
            
            return cell
        case let .note(note):
            let identifier = "NoteCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! NoteCell
            
            if let text = texts[indexPath] {
                cell.setupCellWithString(text)
            }else {
                cell.setupCell(note)
                texts[indexPath] = cell.textPresenter.attributedText
            }
            
            
            return cell
            
        case let .title(title):
            let identifier = "TitleCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! TitleCell
            
            if let text = texts[indexPath] {
                cell.setupCellWithString(text)
            }else {
                cell.setupCell(title)
                texts[indexPath] = cell.textPresenter.attributedText
            }
            
            return cell
            
        case let .metadata(timeLength, daysElapsed):
            let identifier = "MetadataCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! MetadataCell
            cell.setupCell(daysElapsed, timeLength: timeLength)
            return cell
            
        case let .subtitle(subtitle, .fullSize):
            let identifier = "SubtitleCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! SubtitleCell
            
            if let text = texts[indexPath] {
                cell.setupCellWithString(text)
            }else {
                cell.setupCell(subtitle)
                texts[indexPath] = cell.textPresenter.attributedText
            }
            
            return cell
            
        case let .subtitle(subtitle, .centered):
            let identifier = "SubtitleCenteredCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! SubtitleCenteredCell
            
            if let text = texts[indexPath] {
                cell.setupCellWithString(text)
            }else {
                cell.setupCell(subtitle)
                texts[indexPath] = cell.textPresenter.attributedText
            }
            
            return cell
            
        case let .paragraph(text):
            let identifier = "ParagraphCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ParagraphCell
            
            if let text = texts[indexPath] {
                cell.setupCellWithString(text)
            }else {
                cell.setupCell(text)
                texts[indexPath] = cell.textPresenter.attributedText
            }
            
            return cell
            
        case let .list(text, true, index):
            let identifier = "OrderedListCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! OrderedListCell
            
            if let text = texts[indexPath] {
                cell.setupCellWithString(text, itemIndex: index)
            }else {
                cell.setupCell(text, itemIndex: index)
                texts[indexPath] = cell.textPresenter.attributedText
            }
            
            return cell
        case let .list(text, false, _):
            let identifier = "UnorderedListCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! UnorderedListCell
            
            if let text = texts[indexPath] {
                cell.setupCellWithString(text)
            }else {
                cell.setupCell(text)
                texts[indexPath] = cell.textPresenter.attributedText
            }
            
            return cell
            
        }
    }
    
    func getCellNoIndexPath(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        
        switch postBody[indexPath.row] {
        case let .image(imageId, aspectRatio, _, footNote):
            let identifier = "FullSizeImageCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as! ImageCell
            cell.setupCell(imageId, footNote: footNote, aspectRatio: aspectRatio, preloadImage: self.getImageForId(imageId))
            texts[indexPath] = cell.textPresenter.attributedText
            return cell
        case let .link(link, imageId, formattedText, rawText, _, aspectRatio):
            let identifier = "LinkCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as! LinkCell
            cell.setupCell(link, imageId: imageId, formattedText: formattedText, rawText:rawText, aspectRatio: aspectRatio)
            texts[indexPath] = cell.textPresenter.attributedText
            return cell
        case let .note(note):
            let identifier = "NoteCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as! NoteCell
            cell.setupCell(note)
            texts[indexPath] = cell.textPresenter.attributedText
            return cell
            
        case let .title(title):
            let identifier = "TitleCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as! TitleCell
            cell.setupCell(title)
            texts[indexPath] = cell.textPresenter.attributedText
            return cell
            
        case let .metadata(timeLength, daysElapsed):
            let identifier = "MetadataCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as! MetadataCell
            cell.setupCell(daysElapsed, timeLength: timeLength)
            return cell
            
        case let .subtitle(subtitle, .fullSize):
            let identifier = "SubtitleCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as! SubtitleCell
            cell.setupCell(subtitle)
            texts[indexPath] = cell.textPresenter.attributedText
            return cell
            
        case let .subtitle(subtitle, .centered):
            let identifier = "SubtitleCenteredCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as! SubtitleCenteredCell
            cell.setupCell(subtitle)
            texts[indexPath] = cell.textPresenter.attributedText
            return cell
            
        case let .paragraph(text):
            let identifier = "ParagraphCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as! ParagraphCell
            cell.setupCell(text)
            texts[indexPath] = cell.textPresenter.attributedText
            return cell
            
        case let .list(text, true, index):
            let identifier = "OrderedListCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as! OrderedListCell
            cell.setupCell(text, itemIndex: index)
            texts[indexPath] = cell.textPresenter.attributedText
            return cell
        case let .list(text, false, _):
            let identifier = "UnorderedListCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as! UnorderedListCell
            cell.setupCell(text)
            texts[indexPath] = cell.textPresenter.attributedText
            return cell
            
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        switch postBody[indexPath.row] {
            
        case let .image(imageId,_,_,_) : return getImageForId(imageId)?.size.height ?? -48 + 48
        case .link: return 200
        case .note: return 150
        case .title: return 80
        case .metadata: return 60
        case .subtitle:  return 100
        case .paragraph: return 300
        case .list: return 150
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let cell = getCellNoIndexPath(tableView,indexPath: indexPath)
        let size = cell.intrinsicContentSize
        let height = size.height
        return height
    }
}

extension PostReadViewController {
    //Actions
    
    @IBAction func goBack(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func favoriteThisPost(_ sender: AnyObject) {
        if let detailItem = detailItem {
            PostDataAccess.updateValues({ _ in
                detailItem.favorited = !detailItem.favorited
                self.postPreview?.favorited = detailItem.favorited
                self.updateFavoriteIcon()
                
                let action: EventCategory = detailItem.favorited ? EventCategory.favorite_post : EventCategory.unfavorite_post
                GoogleAnalytics.sendEvent(action, action: detailItem.title)
            })
        }
    }
    
    @IBAction func showTags(_ sender: AnyObject) {
        let tagEditor = UIStoryboard(name: "Tags", bundle: nil).instantiateViewController(withIdentifier: "AddTagsViewController") as! AddTagsViewController
        tagEditor.currentPost = detailItem
        present(tagEditor, animated: true, completion: nil)
        GoogleAnalytics.sendEvent(.modify_post_tags, action: detailItem!.title)
    }
    
    
    @IBAction func showConfig(_ sender: AnyObject) {
        let popoverContent = UIStoryboard(name: "Popovers", bundle: nil).instantiateViewController(withIdentifier: "PostOptions") as! ArticlePopover
        popoverContent.actionDelegate = self
        popoverContent.modalPresentationStyle = UIModalPresentationStyle.popover
        
        let popover = popoverContent.popoverPresentationController
        
        popoverContent.preferredContentSize = CGSize(width: 100, height: 135)
        popover!.delegate = self
        popover!.sourceView = sender.imageView
        popover!.popoverLayoutMargins = UIEdgeInsetsMake(0, 0, 0, 0)
        popover!.sourceRect = CGRect(x: 10,y: -24,width: 0,height: 0)
        popover!.permittedArrowDirections = .down
        popover!.popoverBackgroundViewClass = PopOverBackgroundView.self
        
        self.present(popoverContent, animated: true, completion: nil)
    }
}


extension PostReadViewController: ArticlePopoverDelegate {
    
    func increaseFont() {
        FontSizeHandler.diferential += 1
        cells = [:]
        tableView.reloadData()
        GoogleAnalytics.sendEvent(.modify_font_size, action: "increase to \(FontSizeHandler.diferential) extra points")
    }
    
    func decreaseFont() {
        FontSizeHandler.diferential -= 1
        cells = [:]
        tableView.reloadData()
        GoogleAnalytics.sendEvent(.modify_font_size, action: "decrease to \(FontSizeHandler.diferential) extra points")
    }
    /**
     Share contents in social media.
     
     */
    
    func sharePost() {
        if
            let link = postPreview?.link,
            //let curatedLink = link.stringByReplacingOccurrencesOfString("?format=json", withString: "")
            let nsUrl = URL(string: link ) {
            //      postPreview?.imageId
            let title = detailItem?.title ?? postPreview?.title ?? ""
            let text = detailItem?.emailSnippet ?? detailItem?.subtitle ?? ""
            
            let activityViewController: UIActivityViewController = UIActivityViewController(
                activityItems: [title, nsUrl, text], applicationActivities: nil)
            
            // Anything you want to exclude
            activityViewController.excludedActivityTypes = [
                UIActivityType.postToWeibo,
                UIActivityType.print,
                UIActivityType.assignToContact,
                UIActivityType.saveToCameraRoll,
                UIActivityType.postToFlickr,
                UIActivityType.postToVimeo,
                UIActivityType.postToTencentWeibo
            ]
            
            self.present(activityViewController, animated: true, completion: nil)
            GoogleAnalytics.sendEvent(.social_Action_share_post, action: title)
        }
    }
    
    func deletePost() {
        //   let alert = UIAlertController(title: "Esta seguro?", message: "Esta seguro de querer eliminar este articulo? Esto accion no se puede deshacer.", preferredStyle: UIAlertControllerStyle.Alert)
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let deleteAction = UIAlertAction(title: "Eliminar Artículo", style: UIAlertActionStyle.destructive) { (_) in
            self.detailItem?.reportDeleted()
            self.dismiss(animated: true, completion: {
                self.tableView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
                
            })
            GoogleAnalytics.sendEvent(.delete_post, action: self.detailItem!.title)
        }
        let cancel = UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.cancel, handler: nil)
        alert.addAction(cancel)
        alert.addAction(deleteAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension PostReadViewController: CacheImageProvider {
    
    func getImageForId(_ imageID: String) -> UIImage? {
        //        guard let placeHolderImages = placeHolderImages, let image = placeHolderImages[imageID] else { return nil }
        //        return image
        return images?[imageID]
    }
    
    func preloadImage(_ imageId: String, aspectRatio: Double){
        let size = ImageSource.ImageSizes.fullWidth(originalRatio: aspectRatio)
        ImageSource.loadImageAsync(imageId, size: size) { image in
            self.images?[imageId] = image
        }
    }
    
    func preloadImages(_ postBody: [Components]){
        Background {
            self.images = [:]
            for item in postBody {
                switch item {
                case let .image(imageId, aspectRatio, _, _):
                    self.preloadImage(imageId, aspectRatio: aspectRatio)
                default:
                    continue
                }
            }
        }
    }
}

extension PostReadViewController: UIPopoverPresentationControllerDelegate {
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func popoverPresentationController(_ popoverPresentationController: UIPopoverPresentationController, willRepositionPopoverTo rect: UnsafeMutablePointer<CGRect>, in view: AutoreleasingUnsafeMutablePointer<UIView>) {
        let x = popoverPresentationController.presentingViewController.view.center
        let newRect = CGRect(x: x.x , y: x.y, width: 0, height: 0)
        rect.initialize(to: newRect)
    }
}
