//
//  Paragraph.swift
//  Cachivache
//
//  Created by Hugo Alonso on 6/26/16.
//  Copyright © 2016 Hugo Alonso. All rights reserved.
//

import UIKit

class ParagraphCell: ReaderCell {
    override func getDefaultFontSize() -> Int {
        return 14
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textPresenter.contentInset = UIEdgeInsetsMake(-4,0,0,0);
    }
    
    override func getDefaultFontColor() -> String {
        return "#595959"
    }
}







        