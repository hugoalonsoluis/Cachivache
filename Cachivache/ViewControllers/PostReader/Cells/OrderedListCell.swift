//
//  OrderedListCell.swift
//  Cachivache
//
//  Created by Hugo Alonso on 7/7/16.
//  Copyright © 2016 Hugo Alonso. All rights reserved.
//

import Foundation
import UIKit

class OrderedListCell: ReaderCell {
    
    @IBOutlet weak var itemNumber: UILabel!
    
    override func getDefaultFontSize() -> Int {
        return 14
    }
    
    override func getDefaultFontColor() -> String {
        return "#595959"
    }
    
    override func getDefaultFontName() -> String {
        return "FF Kievit Slab"
    }
    
    var readyToPresentNumber : NSAttributedString?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textPresenter.contentInset = UIEdgeInsetsMake(-4,0,0,0);
    }
    
    override func cleanFields() {
        super.cleanFields()
        readyToPresentNumber = nil
        itemNumber.text = nil
        itemNumber.attributedText = nil
    }
    
    func prepareText(_ item: String, itemIndex: Int) {
        readyToPresentNumber = prepareTextForPresentation("\(itemIndex).-")
        super.prepareText(item)
    }
    
    func setupCellWithString(_ item: NSAttributedString, itemIndex: Int){
        readyToPresentNumber = prepareTextForPresentation("\(itemIndex).-")
        super.setupCellWithString(item)
    }
    
    func setupCell(_ item: String, itemIndex: Int) {
        self.prepareText(item, itemIndex: itemIndex)
    }
    
    override func updateUI(){
        Main {
            self.itemNumber.attributedText = self.readyToPresentNumber
        }
        super.updateUI()
    }
}
