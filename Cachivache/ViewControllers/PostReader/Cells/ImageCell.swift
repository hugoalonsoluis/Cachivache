//
//  FullSizeImageCell.swift
//  Cachivache
//
//  Created by Hugo Alonso on 6/26/16.
//  Copyright © 2016 Hugo Alonso. All rights reserved.
//

import UIKit

class ImageCell: ReaderCell {
    
    @IBOutlet weak var fullSizeImage: UIImageView!
    @IBOutlet weak var fullSizeImageHeight: NSLayoutConstraint!
    
    var imageId: String?
    var aspectRatio: Double?
    var preloadImage: UIImage?
    
    override func cleanFields() {
        super.cleanFields()
        fullSizeImage.image = nil
        preloadImage = nil
        //fullSizeImageHeight.constant = 0
    }
    
    override func getDefaultFontName() -> String {
        return "FF Kievit Slab"
    }
    
    override func getDefaultFontSize() -> Int {
        return 14
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textPresenter.contentInset = UIEdgeInsetsMake(-4,0,0,0);
    }
    
    func setupCell(_ imageId: String, footNote: String, aspectRatio: Double, preloadImage: UIImage?) {
        self.preloadImage = preloadImage
        self.prepareText(imageId, footNote: footNote, aspectRatio: aspectRatio)
    }
    
    func setupCellWithAttributedString(_ imageId: String, footNote: NSAttributedString, aspectRatio: Double, preloadImage: UIImage?) {
        self.preloadImage = preloadImage
        setupCellWithString(footNote)
    }
    
    func prepareText(_ imageId: String, footNote: String, aspectRatio: Double) {
        self.aspectRatio = aspectRatio
        self.imageId = imageId
        super.prepareText(footNote)
    }
    
    override func updateUI(){
        if let image = preloadImage {
            updateUIImage(image)
        }else {
            guard let aspectRatio = aspectRatio, let imageId = imageId else { return }
            Main {
                let size = ImageSource.ImageSizes.fullWidth(originalRatio: aspectRatio)
                ImageSource.downloadImageAndSetIn(self.fullSizeImage, imageId: imageId, size: size) { self.updateUIImage($0)
                    self.preloadImage = $0
                }
            }
        }
        super.updateUI()
        
    }
    
    
    func updateUIImage(_ image: UIImage?){
        Main {
            if let image = image {
                self.fullSizeImageHeight.constant = image.size.height
                if self.fullSizeImage.image == nil {
                    self.fullSizeImage.image = image
                }
                UIView.animate(withDuration: 0.15, animations: {
                    self.layoutIfNeeded()
                })
                
            }
        }
    }
}


class FullSizeImageCell: ImageCell {}
class CenteredImageCell: ImageCell {}
