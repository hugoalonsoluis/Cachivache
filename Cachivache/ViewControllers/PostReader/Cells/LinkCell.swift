//
//  Link.swift
//  Cachivache
//
//  Created by Hugo Alonso on 6/26/16.
//  Copyright © 2016 Hugo Alonso. All rights reserved.
//

import UIKit

class LinkCell: ReaderCell {
    
    override func getDefaultFontSize() -> Int {
        return 14
    }
    
    @IBOutlet weak var linkImage: UIImageView!
    
    var link: URL?
    
    override func cleanFields() {
        super.cleanFields()
        linkImage.image = nil
        linkImage.backgroundColor = nil
        link = nil
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.clipsToBounds = true
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.gray.cgColor
    }
    
    
    func navigateURL() {
        NetworkUrl(scheme: nil, page: self.link!.absoluteString).openPage()
        GoogleAnalytics.sendEvent(.open_link, action: self.link!.absoluteString)
    }
    
    func setupCell(_ link:URL, imageId: String, formattedText: String, rawText: String? = nil, mediaResourceId: String? = nil, aspectRatio: Double) {
        self.prepareText(link, imageId: imageId, formattedText: formattedText, rawText: rawText, mediaResourceId: mediaResourceId, aspectRatio: aspectRatio)
        setupImageAndLink(link, imageId: imageId, mediaResourceId: mediaResourceId, aspectRatio: aspectRatio)
    }
    
    func setupCell(_ link:URL, imageId: String, formattedText: NSAttributedString, mediaResourceId: String? = nil, aspectRatio: Double) {
        setupImageAndLink(link, imageId: imageId, mediaResourceId: mediaResourceId, aspectRatio: aspectRatio)
        textPresenter.attributedText = formattedText
    }
    
    override func updateUI(){
        super.updateUI()
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.navigateURL))
        self.addGestureRecognizer(gesture)
    }
    
    override func getDefaultFontName() -> String {
        return "FF Kievit Slab"
    }
    
    func setupImageAndLink(_ link:URL, imageId: String, mediaResourceId: String? = nil, aspectRatio: Double){
        self.link = link
        
        if !imageId.isEmpty {
            let size = ImageSource.ImageSizes.squareLink(originalRatio: aspectRatio)
            ImageSource.downloadImageAndSetIn(linkImage, imageId: imageId, size: size)
        }
        updateUI()
    }
    
    func prepareText(_ link:URL, imageId: String, formattedText: String, rawText: String? = nil, mediaResourceId: String? = nil, aspectRatio: Double){
        setupImageAndLink(link, imageId: imageId, mediaResourceId: mediaResourceId, aspectRatio: aspectRatio)
        super.prepareText(formattedText)
    }
}

