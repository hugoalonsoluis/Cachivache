//
//  SubtitleCell.swift
//  Cachivache
//
//  Created by Hugo Alonso on 6/26/16.
//  Copyright © 2016 Hugo Alonso. All rights reserved.
//

import UIKit

class SubtitleCell: ReaderCell {
    
    override func getDefaultFontSize() -> Int {
        return 16
    }
 
    override func awakeFromNib() {
        super.awakeFromNib()
        textPresenter.contentInset = UIEdgeInsetsMake(-4,0,0,0);
    }
}

