//
//  Metadata.swift
//  Cachivache
//
//  Created by Hugo Alonso on 6/26/16.
//  Copyright © 2016 Hugo Alonso. All rights reserved.
//

import UIKit

class MetadataCell: UITableViewCell {
    
    @IBOutlet weak var daysElapsed: UILabel!
    @IBOutlet weak var timeReading: UILabel!
    
    func cleanFields() {
        daysElapsed.text = nil
        timeReading.text = nil
    }
    
    func setupCell(_ daysAgo: Int, timeLength: Int ) {
        daysElapsed.text = getDaysSincePublished(daysAgo)
        timeReading.text = getReadingTime(timeLength)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        cleanFields()
    }
}

extension MetadataCell {
    
    func getReadingTime(_ timeReading: Int) -> String {
        return "lectura de \(timeReading) min"
    }
    
    func getDaysSincePublished(_ daysAgo: Int) -> String {
        switch daysAgo {
        case 0 :
            return "Hoy"
        case 1 :
            return "Ayer"
        default:
            return "hace \(daysAgo) dias atrás"
        }
    }
}
