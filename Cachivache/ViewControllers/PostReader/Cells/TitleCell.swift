//
//  TitleCell.swift
//  Cachivache
//
//  Created by Hugo Alonso on 6/26/16.
//  Copyright © 2016 Hugo Alonso. All rights reserved.
//

import UIKit

class TitleCell: ReaderCell {
    
    override func getDefaultFontSize() -> Int {
        return 18
    }
}