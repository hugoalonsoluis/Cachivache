//
//  AboutViewController.swift
//  Cachivache
//
//  Created by Hugo Alonso on 7/2/16.
//  Copyright © 2016 Hugo Alonso. All rights reserved.
//

import Foundation
import UIKit

class AboutViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GoogleAnalytics.setScreen("About")
    }
    
    @IBAction func openMedium(_ sender: AnyObject) {
        MenuSocialHandlers.mediumPage.goTo()
        GoogleAnalytics.sendEvent(.social_Action, action: "Open Medium Page")
    }
    
    @IBAction func openFb(_ sender: AnyObject) {
        MenuSocialHandlers.facebookPage.goTo()
        GoogleAnalytics.sendEvent(.social_Action, action:  "Open Facebook Page")
    }
    
    
    @IBAction func openTwiiter(_ sender: AnyObject) {
        MenuSocialHandlers.twitterPage.goTo()
        GoogleAnalytics.sendEvent(.social_Action, action:  "Open Twiiter Page")
    }
    
    @IBAction func openInstagram(_ sender: AnyObject) {
        MenuSocialHandlers.instagramPage.goTo()
        GoogleAnalytics.sendEvent(.social_Action, action:  "Open Instagram Page")
    }
    @IBAction func openMail(_ sender: AnyObject) {
        MenuSocialHandlers.sendMail(self)
        GoogleAnalytics.sendEvent(.social_Action, action:  "Send Mail")
    }
}
