//
//  MediumPostReader.swift
//  Cachivache
//
//  Created by Hugo Alonso on 6/25/16.
//  Copyright © 2016 Hugo Alonso. All rights reserved.
//

import Foundation

class MediumPostReader {
    
    let post: MediumPost
    
    init(post: MediumPost) {
        self.post = post
    }
    
    func getComponents() -> [Components]? {
        guard let paragraphs = post.content?.bodyModel?.paragraphs else { return nil }
        return extractComponentsFromText(paragraphs)
    }
    
    fileprivate enum MarkupType: Int {
        case strong = 1
        case italyc = 2
        case link   = 3
        
        func htmlRepresentation() -> String {
            switch self {
            case .strong: return "strong"
            case .italyc: return "em"
            case .link: return "a"
            }
        }
        func applyMarkup(_ text: String, href: String = "") -> String {
            let start: String
            switch self {
            case .link: start = startString(href)
            default: start = startString()
            }
            return start + text + endString()
        }
        
        func startString(_ href: String? = "") -> String {
            switch self {
            case .link: return "<a href= \"" + href! + "\">"
                
            default: return "<\(htmlRepresentation())>"
            }
        }
        
        func endString() -> String {
            return "</\(htmlRepresentation())>"
        }
    }
    
    fileprivate var orderedListIndex = 0
    
    fileprivate func interpretParagraph(_ paragraph: Paragraph) -> Components? {
        let text = interpretText(paragraph)
        
        if paragraph.type == 10 {
            orderedListIndex = orderedListIndex.advanced(by: 1)
        }else {
            orderedListIndex = 0
        }
        
        switch paragraph.type! {
        //images
        case 4:
            // var layout = Layout.Centered
            // if paragraph.layout == 5 {
            let layout = Layout.fullSize
            // }
            guard
                //  "metadata":{"id":"1*Ro0BJL_-VQdT3X27Et7GpQ.jpeg","originalWidth":2076,"originalHeight":1038}},
                let image = paragraph.metadata?.id,
                let width = paragraph.metadata?.originalWidth,
                let height = paragraph.metadata?.originalHeight
                else { return nil }
            let aspectRatio : Double = Double(width) / Double(height)
            return Components.image(imageId: image, aspectRatio: aspectRatio, layout: layout, footNote: text)
        //     type   case 3: This post name
        case 3:  return Components.title(title: text)
        case 2:  return Components.note(note: text)
            
        case 9:  return  Components.list(item: text, ordered: false, index: 0)
        case 10: return Components.list(item: text, ordered: true, index: orderedListIndex)
            
        case 7:  return Components.subtitle(subtitle: text, layout: .centered)
        //     type   case 13: this post subtitle
        case 13: return Components.subtitle(subtitle: text, layout: .fullSize)
        case  6: return Components.note(note: text)
        //     type   case 14: Are the links (What this method is looking for)
        case 14:
            guard
                let thumbnailImageId = paragraph.mixtapeMetadata?.thumbnailImageId,
                let href = paragraph.mixtapeMetadata?.href,
                let mediaResourceId = paragraph.mixtapeMetadata?.mediaResourceId,
                let nsurl = URL(string: href),
                let rawText = paragraph.text,
                let hostUrl = nsurl.host
                else { return nil }
            
            var text = ""
            
            let parts = rawText.components(separatedBy: "\n")
            text.append("<strong>")
            
            let title = parts.first?.replacingOccurrences(of: "\\", with: "") ?? ""
            
            text.append(title)
            text.append("</strong>")
            text.append("<br/>")
            text.append("<em>")
            
            let postSubtitle = parts.last?.replacingOccurrences(of: hostUrl, with: "").replacingOccurrences(of: "\\", with: "") ?? ""
            
            text.append(postSubtitle)
            text.append("</em>")
            
            return Components.link(link: nsurl, imageId: thumbnailImageId, formattedText: text, rawText: rawText, mediaResourceId: mediaResourceId, aspectRatio: 1.12)
        case 11: //these are embedded content --Add something, image, text..?
            guard
                let mediaResourceId = paragraph.iframe?.mediaResourceId,
                let nsurl = URL(string: "https://cachivachemedia.com/media/\(mediaResourceId)")
                else { return nil }
            
            return Components.link(link: nsurl, imageId: "", formattedText: "<strong>Abrir contenido adjunto...</strong>", rawText: "Abrir contenido adjunto...", mediaResourceId: mediaResourceId, aspectRatio: 1.12)
            
        default : return Components.paragraph(text: text)
        }
    }
    
     func interpretText(_ paragraph: Paragraph, ignore_href: Bool = false) -> String {
        if var newString = paragraph.text, let markups = paragraph.markups {
            for markup in markups.reversed() {
                if let type = MarkupType(rawValue: markup.type!) {
                    
                    let endLink = newString.characters.index(newString.startIndex, offsetBy: markup.end!)
                    let startLink = newString.characters.index(newString.startIndex, offsetBy: markup.start!)
                    
                    if type == .link {
                        if ignore_href { continue }
                        /*
                        do { //Adjust to full word
                            
                            
                            
                            loopend: while (String.distance(from: endLink, to: newString.characters.endIndex) > 0) {
                                let chars = newString.characters[endLink]
                                //     print(chars)
                                switch chars {
                                case  " ", "<",",",".":
                                    //endLink = endLink.advancedBy(1)
                                    break loopend
                                case "]": break loopend
                                default: endLink = <#T##Collection corresponding to `endLink`##Collection#>.index(endLink, offsetBy: 1)
                                }
                            }
                            
                            loopstart: while (newString.characters.distance(from: newString.characters.startIndex, to: startLink) > 0) {
                                let chars = newString.characters[startLink]
                                switch chars {
                                case " ", ">", ".", ",":
                                    startLink = <#T##Collection corresponding to `startLink`##Collection#>.index(startLink, offsetBy: 1)
                                    break loopstart
                                case "[": break loopstart
                                default: startLink = <#T##Collection corresponding to `startLink`##Collection#>.index(startLink, offsetBy: -1)
                                }
                            }
                        }*/
                        //     newString.insertContentsOf(type.endString().characters, at: endLink)
                        //     newString.insertContentsOf(type.startString(markup.href).characters, at: startLink)
                    }
 
                    newString.insert(contentsOf: type.endString().characters, at: endLink)
                    newString.insert(contentsOf: type.startString(markup.href ?? "").characters, at: startLink)
                }
                
            }
            return newString
        }
        
        return ""
    }
    
    
    func extractComponentsFromText(_ paragraphs: [Paragraph]) -> [Components]? {
        var article = [Components]()
        
        //Add author if missing
        var authorAdded = false
        
        for paragraph in paragraphs {
            if let component = interpretParagraph(paragraph){
                 //Add author if missing
                if !authorAdded {
                    switch component {
                    case .paragraph(let text):
                        if !text.contains("Por:") {
                            if let author = post.user?.name, !author.isEmpty && !author.contains("Cachivache") {
                                let authorComponent = Components.paragraph(text: "<strong>Por: \(author)</strong>")
                                article.append(authorComponent)
                            }
                        }
                        authorAdded = true
                    default:
                        break
                    }
                }
                
                article.append(component)
            }
        }
        //  print(fullText)
        return article
    }
    
    struct PostVirtuals {
        var wordCount = 0
        var readingTime = 0.0
        var emailSnippet = ""
        var airDate : Date? = nil
        var subtitle = ""
        var imageId = ""
        var imageAspectRatio : Double = 0
        var tags = [String]()
    }
    
    func extractVirtualContent(_ virtuals: Virtuals) -> PostVirtuals {
        var postVirtuals = PostVirtuals()
        postVirtuals.wordCount = virtuals.wordCount ?? 0
        postVirtuals.readingTime = virtuals.readingTime ?? 0.0
        postVirtuals.emailSnippet = virtuals.emailSnippet ?? ""
        postVirtuals.subtitle = virtuals.subtitle ?? ""
        postVirtuals.imageId = virtuals.previewImage?.imageId ?? ""
        
        if let height = virtuals.previewImage?.originalHeight, let width = virtuals.previewImage?.originalWidth {
            postVirtuals.imageAspectRatio = Double(height / width)
        }
        
        postVirtuals.airDate = {
            
            let s_date = virtuals.createdAtEnglish ?? virtuals.acceptedAtEnglish ?? virtuals.firstPublishedAtEnglish ??   virtuals.updatedAtEnglish ?? virtuals.latestPublishedAtEnglish
            
            let date : Date?
            if let s_date = s_date {
                 date = Date(dateString: s_date, shortFormat: true)
            }else {
                date = nil
            }
            return date
        }()
        
        postVirtuals.tags = {
            var tags = [String]()
            
            for tag in virtuals.tags! {
                tags.append(tag.name!)
            }
            return tags
        }()
        return postVirtuals
    }
    
}
