//
//  AllModels.swift
//  Cachivache
//
//  Created by Hugo Alonso on 6/9/16.
//  Copyright © 2016 Hugo Alonso. All rights reserved.
//

import Foundation
import ObjectMapper

class RequestEnclosure: Mappable {
    var success: Bool?
    var payload: Payload?
    
    //MARK: Mappable protocol
    required init?( map: Map) {
    }
    
    func mapping(map: Map) {
        success <- map["success"]
        payload <- map["payload"]
    }

}


class Payload: Mappable {
    var value: MediumPost?
    var references: MediumReferences?
    
    //MARK: Mappable protocol
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        value <- map["value"]
        references <- map["references"]
    }

}

class MediumReferences: Mappable {
    var user: [String: User]?
    
    //MARK: Mappable protocol
    required init?(map: Map) {
    }
    
    func mapping( map: Map) {
        user <- map["User"]
    }
}
//
//class UserReferences: Mappable {
//    var user: User?
//    
//    //MARK: Mappable protocol
//    required init?(_ map: Map) {
//    }
//    
//    func mapping(map: Map) {
//        user <- map.value()!
//    }
//}
/*
 "User": {
 
 "42b5c5b05070": {
 "userId": "42b5c5b05070",
 "name": "Fernando Medina Fernández",
 "username": "fnandophoto",
 "createdAt": 1456100750776,
 "lastPostCreatedAt": 1470606878516,
 "imageId": "1*4hHfhsbFsFfS9bjswj84OQ.jpeg",
 "backgroundImageId": "",
 "bio": "En busca de ese instante…",
 "twitterScreenName": "FnandoMedina",
 */
class User: Mappable {
    var userId: String?
    var name: String?
    var username: String?
    var imageId: String?
    var bio: String?
    //MARK: Mappable protocol
    required init?( map: Map) {
    }
    
    func mapping(map: Map) {
        userId <- map["userId"]
        name <- map["name"]
        username <- map["username"]
        imageId <- map["imageId"]
        bio <- map["bio"]
    }
}


class MediumPost: Mappable {
    var id: String?
    var versionId: String?
    var creatorId: String?
 //   var homeCollectionId: String?
    var title: String?
 //   var detectedLanguage: String?
    var latestVersion: String?
    var latestPublishedVersion: String?
    var createdAt: Double?
    
    lazy var createdAtDate: Date? = {
         guard let timelapse = self.createdAt, timelapse > 0 else { return nil }
        return Date(timeIntervalSince1970: timelapse/1000)
    }()
    
    var updatedAt: Double?
    
    lazy var updatedAtDate: Date? = {
        guard let timelapse = self.updatedAt, timelapse > 0 else { return nil }
        return Date(timeIntervalSince1970: timelapse/1000)
    }()
    
    var acceptedAt: Double?
    
    lazy var acceptedAtDate: Date? = {
        guard let timelapse = self.acceptedAt, timelapse > 0 else { return nil }
        return Date(timeIntervalSince1970: timelapse/1000)
    }()
    
    var firstPublishedAt: Double?
    
    lazy var firstPublishedAtDate: Date? = {
        guard let timelapse = self.firstPublishedAt, timelapse > 0 else { return nil }
        return Date(timeIntervalSince1970: timelapse/1000)
    }()
    
    var latestPublishedAt: Double?
    
    lazy var latestPublishedAtDate: Date? = {
        guard let timelapse = self.latestPublishedAt, timelapse > 0 else { return nil }
        return Date(timeIntervalSince1970: timelapse/1000)
    }()
    
    var isRead: Bool?
 //   var vote: Bool?
 //   var displayAuthor: String?
    var content: Content?
    var virtuals: Virtuals?
 //   var coverless: Bool?
 //   var isTitleSynthesized: Bool?
 //   var visibility: Int?
    
    var slug: String?
    var uniqueSlug: String?
    
    var previewContent: PreviewContent?
    //The full urls
    var canonicalUrl: String?
    var webCanonicalUrl: String?
    var mediumUrl: String?
    //This should be Post
    var type: String?
    
    
    var user: User?
    init() {}
    
    //MARK: Mappable protocol
    required init?( map: Map) {
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        versionId <- map["versionId"]
        title <- map["title"]
        latestVersion <- map["latestVersion"]
        latestPublishedVersion <- map["latestPublishedVersion"]
        createdAt <- map["createdAt"]
        updatedAt <- map["updatedAt"]
        acceptedAt <- map["acceptedAt"]
        firstPublishedAt <- map["firstPublishedAt"]
        latestPublishedAt <- map["latestPublishedAt"]
        isRead <- map["isRead"]
        content <- map["content"]
        virtuals <- map["virtuals"]
        slug <- map["slug"]
        uniqueSlug <- map["uniqueSlug"]
        previewContent <- map["previewContent"]
        canonicalUrl <- map["canonicalUrl"]
        webCanonicalUrl <- map["webCanonicalUrl"]
        mediumUrl <- map["mediumUrl"]
        type <- map["type"]
        creatorId <- map["creatorId"]
    }

}

class MediumUsers: Mappable {
    
    
    //MARK: Mappable protocol
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
       
    }

}

class Tag: Mappable {
    var slug: String?
    var name: String?
    var postCount: Int?
    var virtuals: [TagVirtual]?
    
    //MARK: Mappable protocol
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        slug <- map["slug"]
        name <- map["name"]
        postCount <- map["postCount"]
        virtuals <- map["virtuals"]
    }

}

class TagVirtual: Mappable {
    var isFollowing : Bool?
    var metadata: TagMetadata?
    //This should be tag
    var type: String?
    
    //MARK: Mappable protocol
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        isFollowing <- map["isFollowing"]
        metadata <- map["metadata"]
        type <- map["type"]
    }

}

class TagMetadata: Mappable {
    var followerCount: Int?
    var postCount: Int?
    var coverImage: TagImage?
    
    //MARK: Mappable protocol
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        followerCount <- map["followerCount"]
        postCount <- map["postCount"]
        coverImage <- map["coverImage"]
    }

}
class TagImage: Mappable {
    var id: String?
    var originalWidth: Int?
    var originalHeight: Int?
    
    //MARK: Mappable protocol
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        originalWidth <- map["originalWidth"]
        originalHeight <- map["originalHeight"]
    }

}

class MediumImage: Mappable {
    var imageId: String?
    var filter: String?
    var backgroundSize: String?
    var originalWidth: Int?
    var originalHeight: Int?
    var strategy: String?
    var height: Int?
    var width: Int?
    
    //MARK: Mappable protocol
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        imageId <- map["imageId"]
        filter <- map["filter"]
        backgroundSize <- map["backgroundSize"]
        originalWidth <- map["originalWidth"]
        originalHeight <- map["originalHeight"]
        strategy <- map["strategy"]
        height <- map["height"]
        width <- map["width"]
    }

}

class Links: Mappable {
    var entries: Link?
    var version: String?
    
    //MARK: Mappable protocol
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        entries <- map["entries"]
        version <- map["version"]
       
    }

}

class Link: Mappable {
    var url: String?
   // var alts: [AltLink]?
    
    //MARK: Mappable protocol
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        url <- map["url"]
  //      alts <- map["alts"]
    }
}

//class AltLink: Mappable {
//    var url: String?
//    
//    var type : ​Int?
//    
//    //MARK: Mappable protocol
//    required init?(_ map: Map) {
//    }
//    
//    func mapping(map: Map) {
//        url <- map["url"]
//        type <- map["type"]
//    }
//
//}

class Virtuals: Mappable {
 //   var createdAtRelative: String?
 //   var updatedAtRelative: String?
 //   var acceptedAtRelative: String?
    var createdAtEnglish: String?
    var updatedAtEnglish: String?
    var acceptedAtEnglish: String?
    var firstPublishedAtEnglish: String?
    var latestPublishedAtEnglish: String?
 //   var allowNotes: Bool?
    var snippet: String?
    
    var previewImage: MediumImage?
    var wordCount: Int?
    var imageCount: Int?
    var readingTime: Double?
    
    var subtitle: String?
 //   var usersBySocialRecommends: [MediumUsers]?
    
 //   var latestPublishedAtAbbreviated: String?
 //   var firstPublishedAtAbbreviated: String?
    var emailSnippet: String?
    
    var recommends : Int?
 //   var isBookmarked: Bool?
    var tags : [Tag]?
    
    var socialRecommendsCount : Int?
    var responsesCreatedCount : Int?
    
    var links: [Links]?
 //   var isLockedPreviewOnly: Bool?
    
    //MARK: Mappable protocol
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        createdAtEnglish <- map["createdAtEnglish"]
        updatedAtEnglish <- map["updatedAtEnglish"]
        acceptedAtEnglish <- map["acceptedAtEnglish"]
        firstPublishedAtEnglish <- map["firstPublishedAtEnglish"]
        latestPublishedAtEnglish <- map["latestPublishedAtEnglish"]
        snippet <- map["snippet"]
        previewImage <- map["previewImage"]
        wordCount <- map["wordCount"]
        imageCount <- map["imageCount"]
        readingTime <- map["readingTime"]
        subtitle <- map["subtitle"]
        
        emailSnippet <- map["emailSnippet"]
        recommends <- map["recommends"]
        
        tags <- map["tags"]
        socialRecommendsCount <- map["socialRecommendsCount"]
        responsesCreatedCount <- map["responsesCreatedCount"]
        links <- map["links"]
        
    }


}

class PreviewContent: Mappable {
    var bodyModel : BodyModel?
    //var isFullContent: Bool
    
    //MARK: Mappable protocol
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        bodyModel <- map["bodyModel"]
    }

}

class Content: Mappable {
    var subtitle: String?
    var bodyModel : BodyModel?
    //var postDisplay:
    
    //MARK: Mappable protocol
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        subtitle <- map["subtitle"]
        bodyModel <- map["bodyModel"]
    }

}

class BodyModel: Mappable {
    var paragraphs : [Paragraph]?
    var sections: [Sections]?
    
    //MARK: Mappable protocol
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        paragraphs <- map["paragraphs"]
        sections <- map["sections"]
    }

}


class MixtapeMetadata: Mappable {
    var mediaResourceId : String?
    var thumbnailImageId: String?
    var href: String?
    
    //MARK: Mappable protocol
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        mediaResourceId <- map["mediaResourceId"]
        thumbnailImageId <- map["thumbnailImageId"]
        href <- map["href"]
    }

}

class ParagraphMetadata: Mappable {
    var id : String?
    var originalWidth: Int?
    var originalHeight: Int?
    
    //MARK: Mappable protocol
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        originalWidth <- map["originalWidth"]
        originalHeight <- map["originalHeight"]
    }
    
}

class Iframe: Mappable {
    var mediaResourceId : String?
    var iframeWidth: Int?
    var iframeHeight: Int?
    
    //MARK: Mappable protocol
    required init?( map: Map) {
    }
    
    func mapping(map: Map) {
        mediaResourceId <- map["mediaResourceId"]
        iframeWidth <- map["iframeWidth"]
        iframeHeight <- map["iframeHeight"]
    }
    
}

class Paragraph: Mappable {
    var name : String?
    var type : Int?
    var text : String?
    var markups : [Markup]?
    var alignment : Int?
    var layout: Int?
    var iframe: Iframe?
    var mixtapeMetadata: MixtapeMetadata?
    var metadata: ParagraphMetadata?
    
    //MARK: Mappable protocol
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        type <- map["type"]
        text <- map["text"]
        layout <- map["layout"]
        markups <- map["markups"]
        alignment <- map["alignment"]
        iframe <- map["iframe"]
        mixtapeMetadata <- map["mixtapeMetadata"]
        metadata <- map["metadata"]
    }

}

class Markup: Mappable {
    var type : Int? //3 link, 2, 1 unic
    var start : Int?
    var end : Int?
    var href: String?
    var title: String?
    var rel: String?
    var anchorType: Int?
    
    //MARK: Mappable protocol
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        type <- map["type"]
        start <- map["start"]
        end <- map["end"]
        href <- map["href"]
        title <- map["title"]
        rel <- map["rel"]
        anchorType <- map["anchorType"]
    }

}
class Sections: Mappable {
    var startIndex: Int?
    var name: String?
    
    //MARK: Mappable protocol
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        startIndex <- map["startIndex"]
        name <- map["name"]
    }

}
/*
 "{\"success\":false,\"error\":\"Session has changed\",\"v\":3,\"b\":\"22640-3cf844e\",\"errorInfo\":{\"code\":3000}}"
 */
class MediumError: Mappable {
    var success: Bool?
    var error: String?
    var errorInfo: ErrorInfo?
    
    //MARK: Mappable protocol
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        success <- map["success"]
        error <- map["error"]
        errorInfo <- map["errorInfo"]
    }

}

class ErrorInfo: Mappable {
    var code: Int?
    
    //MARK: Mappable protocol
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
       code <- map["code"]
    }
    
}
