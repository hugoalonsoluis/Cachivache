//
//  Components.swift
//  Cachivache
//
//  Created by Hugo Alonso on 6/24/16.
//  Copyright © 2016 Hugo Alonso. All rights reserved.
//

import Foundation
//enum Components {
//    case Title(title:NSAttributedString)
//    case Note(note: NSAttributedString)
//    case Link(link:NSURL, imageId: String, linkText: NSAttributedString)
//    case Paragraph(text: NSAttributedString)
//    case Metadata(timeLength:Int, daysElapsed: Int)
//    case FullSizeImage(imageId: String)
//}

enum Layout {
    case fullSize, centered
}

enum Components  {
    case title(title:String)
    case note(note: String)
    case list(item: String, ordered:Bool, index: Int)
    case subtitle(subtitle:String, layout: Layout)
    case link(link:URL, imageId: String, formattedText: String, rawText: String, mediaResourceId: String, aspectRatio: Double)
    case paragraph(text: String)
    case metadata(timeLength:Int, daysElapsed: Int)
    case image(imageId: String, aspectRatio: Double, layout: Layout, footNote: String)
    
    static func buildComponentFrom(rawRepresentation raw: RawComponents) -> Components{
        switch raw.type {
        case 0:  return Components.title(title: raw.text)
        case 1:  return Components.note(note: raw.text)
        case 2:  return Components.subtitle(subtitle: raw.text, layout: raw.fullSize ? .fullSize : .centered)
        case 3:  return Components.link(link: URL(string: raw.link)!, imageId: raw.imageId, formattedText: raw.formattedText, rawText: raw.text, mediaResourceId: raw.mediaResourceId, aspectRatio: raw.aspectRatio)
        case 4:  return Components.paragraph(text: raw.text)
        case 5:  return Components.metadata(timeLength: raw.timeLength, daysElapsed: raw.daysElapsed)
        case 6:  return Components.image(imageId: raw.imageId, aspectRatio: raw.aspectRatio, layout: raw.fullSize ? .fullSize : .centered, footNote: raw.text)
        case 7:  return Components.list(item: raw.text, ordered: raw.orderedList, index: raw.listItemIndex)
        default:
            fatalError("Not implemeted")
        }
    }
    
    static func getRawComponentFrom(_ component: Components) -> RawComponents {
        switch component {
        case let .title(text):  return RawComponents(type: 0, text: text)
        case let .note(text):  return RawComponents(type: 1, text: text)
        case let .subtitle(text, layout):  return RawComponents(type: 2, text: text, fullSize: layout == .fullSize ? true : false)
        case let .link(link, imageId, formattedText, rawText, mediaResourceId, aspectRatio):  return RawComponents(type: 3, link: link.absoluteString, imageId: imageId, formattedText: formattedText, rawText: rawText, mediaResourceId: mediaResourceId, aspectRatio: aspectRatio)
        case let .paragraph(text):  return RawComponents(type: 4, text: text)
        case let .metadata(timeLength, daysElapsed):  return RawComponents(type: 5, timeLength: timeLength, daysElapsed: daysElapsed)
        case let .image(imageId, aspectRatio, layout, footNote):  return RawComponents(type: 6, imageId: imageId, aspectRatio: aspectRatio, fullSize: layout == .fullSize ? true : false, footNote: footNote)
        case let .list(text, ordered, listItemIndex):  return RawComponents(type: 7, text: text, ordered: ordered, index: listItemIndex)
        }
    }
}

