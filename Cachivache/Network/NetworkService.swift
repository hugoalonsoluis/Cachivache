//
//  CharacterService.swift
//  Marvel Characters
//
//  Created by Hugo on 5/26/16.
//  Copyright © 2016 halonsoluis. All rights reserved.
//

import RxAlamofire
import RxSwift
import ObjectMapper
import RxCocoa
import Result

class NetworkService {
    fileprivate lazy var rx_params: Observable<[String:String]> = self.getParams()
  //  private lazy var rx_route: Observable<Routes> = Observable.empty()
    
    init() {
    }
    
    fileprivate func getParams() -> Observable<[String:String]> {
        return Observable.just(APIHandler.getDefaultParamsAsDict())
    }
    
    func getData(_ route: Routes, forPreviewPost: FeedNetworkService.PreviewPost?) -> Observable<Result<MediumPost,RequestError>>  {
        return rx_params
            .subscribeOn(MainScheduler.instance)
            .do(onNext: { (_) in
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }, onError: nil, onCompleted: nil, onSubscribe: nil, onDispose: nil)
            .flatMapLatest { parameters in
                return RxAPICaller.requestWithParams(parameters, route: route, forPreviewPost: forPreviewPost)
            }
            ///.retry(0)
            .observeOn(MainScheduler.instance)
            .do(onNext: { (_) in
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }, onError: nil, onCompleted: nil, onSubscribe: nil, onDispose: nil)
            .catchError({ (error) -> Observable<Result<MediumPost,RequestError>> in
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                return Observable.just(Result.failure(RequestError.error("There are problems connecting with the server. Please try again later.")))
            })
    }
    
}
