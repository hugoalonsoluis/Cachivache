//
//  MockupNetworking.swift
//  Marvel Characters
//
//  Created by Hugo Alonso on 5/27/16.
//  Copyright © 2016 halonsoluis. All rights reserved.
//

import Foundation

//Mark: Network Access Mockup
struct MockupResourceLoader {

    fileprivate init() {}
    
    static func getMockupData(_ route: Routes) -> Data? {
        let locator : (path:String, ext: String) = route.getMockupRoute()
     //   dispatch_main().wait
        return loadFileData(locator.path, ext: locator.ext)
    }
        
    fileprivate static func loadFileData(_ name:String, ext: String) -> Data? {
        let bundle = Bundle.main
        
        guard
            let path = bundle.path(forResource: name, ofType: ext) ,
            let data = try? Data(contentsOf: URL(fileURLWithPath: path)) else {
                
                return nil
        }
        return data
    }
}
