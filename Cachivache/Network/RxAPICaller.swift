//
//  RxAPICaller.swift
//  Marvel Characters
//
//  Created by Hugo Alonso on 5/27/16.
//  Copyright © 2016 halonsoluis. All rights reserved.
//

import RxSwift
import RxAlamofire
import Result
import ObjectMapper

struct RxAPICaller {
    
    fileprivate static let mockupEnabled : Bool = { return ProcessInfo.processInfo.arguments.contains("MOCKUP_MODE") }()
    
    static func requestWithParams(_ parameters: [String:String], route: Routes, forPreviewPost: FeedNetworkService.PreviewPost?) -> Observable<Result<MediumPost,RequestError>> {
        
        switch mockupEnabled {
        case true:   return requestMockupData(parameters, route: route, forPreviewPost: forPreviewPost)
        case false:  return RxAPICaller.requestNetworkData(parameters, route: route, forPreviewPost: forPreviewPost)
        }
    }
    
    static fileprivate func requestMockupData(_ parameters: [String:String], route: Routes, forPreviewPost: FeedNetworkService.PreviewPost?) -> Observable<Result<MediumPost,RequestError>> {
        
       
        guard let data = MockupResourceLoader.getMockupData(route), let string = String(data: data, encoding: String.Encoding.utf8) else {
            return Observable.just(Result.failure(RequestError.mediumServer("Error while accessing Mockup Data", forUrl: route.getMockupRoute(), using: forPreviewPost)))
        }
        
        guard let post = extractPostData(string) else {
            return Observable.just(Result.failure(RequestError.parser("Error while parsing Mockup Data")))
        }
        
        return Observable.just(Result.success(post))
    }
    
    fileprivate static func requestNetworkData(_ parameters: [String:String], route: Routes, forPreviewPost: FeedNetworkService.PreviewPost?) -> Observable<Result<MediumPost,RequestError>> {
        print(route.getRoute())
        return RxAlamofire
            .requestData(.get, route.getRoute(), parameters: parameters, headers: ["Content-Type" : "application/json; charset=utf-8"])
            .retry(2)
          //  .debug()
          //  .doOn() { /*print("\(route.getRoute()) with params \(parameters)") */}
            .observeOn(ConcurrentDispatchQueueScheduler.init(queue: DispatchQueue.global(qos: DispatchQoS.QoSClass.background)))
            .flatMapLatest { (response: HTTPURLResponse, data: Data) -> Observable<Result<MediumPost,RequestError>> in
                
                
                guard response.statusCode == 200 else {
                    
                    guard let string = String(data: data, encoding: String.Encoding.utf8), let error = extractErrorData(string), let message = error.error else {
                        return Observable.just(Result.failure(RequestError.parser("Error while parsing an error data")))
                    }
                   // print(message)
                    return Observable.just(Result.failure(RequestError.mediumServer(message, forUrl: route.getRoute(), using: forPreviewPost)))
                }
                let string = String(data: data, encoding: String.Encoding.utf8)!
                guard let post = extractPostData(string) else {
                    return Observable.just(Result.failure(RequestError.parser("Error while parsing post data")))
                }
                print("Successfully downloaded \(post.title!)")
                return Observable.just(Result.success(post))
        }.catchError({ (error) -> Observable<Result<MediumPost, RequestError>> in
            return Observable.just(Result.failure(RequestError.timeout))
        })
    }
    
    fileprivate static func extractErrorData(_ mediumData: String) -> MediumError? {
      return extractData(mediumData)
    }
    
    fileprivate static func extractPostData(_ mediumData: String) -> MediumPost? {
        let box: RequestEnclosure? = extractData(mediumData)
        if let mediumPost = box?.payload?.value {
            if let creatorId = mediumPost.creatorId, let user = box?.payload?.references?.user?[creatorId] {
                mediumPost.user = user
            }
            return mediumPost
        }
        return nil
    }
    
    fileprivate static func extractData<T:Mappable>(_ mediumData: String) -> T? {
        let string = removeSecurityString(mediumData)
      //  string = string.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.letterCharacterSet())!
        if let string = string.removingPercentEncoding {
            return Mapper<T>().map(JSONString: string)
        }
        return Mapper<T>().map(JSONString: string)
    }
    
    fileprivate static func removeSecurityString(_ string:String) -> String {
        let securityString = "])}while(1);</x>"
        return string.replacingOccurrences(of: securityString, with: "")
    }
    
}
