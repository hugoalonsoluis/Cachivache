//
//  Routes.swift
//  Marvel Characters
//
//  Created by Hugo Alonso on 5/25/16.
//  Copyright © 2016 halonsoluis. All rights reserved.
//

/**
 Store all the URLs used for networking requests
 
 */

import Foundation

enum Routes {
    fileprivate static let allInOne = "https://medium.com/@Cachivache/cachivache-media-en-un-solo-lugar-f69fa90daf5c"
    fileprivate static let imageServiceAddress = "https://d262ilb51hltx0.cloudfront.net/fit/c"
    fileprivate static let baseURL = "https://medium.com/"
 
    static let instagram = "cachivachemedia"
    static let fb = "cachivachemedia"
    static let medium = "https://cachivachemedia.com"
    static let twiter = "cachivachemedia"
    static let mail = "cachivachemedia@gmail.com"
    
    case allPost
    case directRoute(url: String)
    case imageUrl(id:String, width: Int, height: Int)
   
    func getRoute() -> String {
        switch self {
        case .allPost: return Routes.allInOne
        case let .directRoute(url: url) :return url
        case let .imageUrl(id: imageID, width: width, height: height) :return "\(Routes.imageServiceAddress)/\(width)/\(height)/\(imageID)"
        }
    }
}

func random(from0To: Int) -> Int {
    return Int(arc4random_uniform(UInt32(from0To)))
}


//To request Mockup Data
extension Routes {
    func getMockupRoute() -> String {
        let route: (String, String) = getMockupRoute()
        return route.0 + "." + route.1
    }
    func getMockupRoute() -> (path:String, ext: String){
        switch self {
        case .allPost:      return (path: "allinOne", ext: "json")
        case .directRoute:  return (path: "MediumArticle\(random(from0To: 5))", ext: "json")
        case .imageUrl:     return (path: "postImageExample", ext: "jpg")
        }
    }
}
