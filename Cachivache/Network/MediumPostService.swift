//
//  PostService.swift
//  Cachivache
//
//  Created by Hugo Alonso on 6/9/16.
//  Copyright © 2016 Hugo Alonso. All rights reserved.
//


import Foundation
import Result
import RxSwift
import RxCocoa

class MediumPostService {
    
    let pendingForDownload = Variable<[FeedNetworkService.PreviewPost]>([])
    let downloadedPost = Variable<MediumPost?>(nil)
    let postToDownload: [FeedNetworkService.PreviewPost]
    
    fileprivate let disposeBag = DisposeBag()
    
    init?(newPosts: [FeedNetworkService.PreviewPost]) {
        guard !newPosts.isEmpty else { return nil}
        postToDownload = newPosts
        prepareSubscribers()
    }
    
    func startDownloading() {
        initialiseQueue()
    }
    
    fileprivate func initialiseQueue() {
        reportStartDownloading()
        pendingForDownload.value = postToDownload
    }
    
    func prepareSubscribers() {
        
        downloadedPost
            .asObservable()
            .subscribeOn(MainScheduler.instance)
            .bindNext { (post) in
                guard
                    let post = post
                    else { return }
                
                _ = self.saveInDB(post)
                
            }.addDisposableTo(disposeBag)
        
        pendingForDownload
            .asObservable()
            .filter { if $0.isEmpty { self.reportDoneDownloading(); return false }; return true}
            .flatMapLatest { Driver.just((link:$0.last?.link, post:$0.last)) }
            .filter {
                guard let link = $0.link, let post = PostDataAccess.findByLink(link)
                    else {  return true  }
               
                self.pendingForDownload.value.removeLast()
                
                if !post.allImagesPrecached {
                    self.precacheImagesFor(post: post)
                }
                return false
            }
            .flatMap { MediumPostService.getMediumPost($0.link!, forPreviewPost: $0.post!) }
            .bindNext {  self.downloadedPost.value = $0; self.pendingForDownload.value.removeLast() }
            .addDisposableTo(disposeBag)
    }
    
    
    func extractMeaningFulData(_ post: MediumPost) -> (title: String, link: String, MediumPostReader.PostVirtuals, [Components])? {
        let postsReader = MediumPostReader(post: post)
        guard
            let title = post.title,
            let link = post.webCanonicalUrl,
            let postVirtuals = post.virtuals,
            let components = postsReader.getComponents()
            else { return nil }
        var virtuals = postsReader.extractVirtualContent(postVirtuals)
        virtuals.airDate = post.createdAtDate
        return (title: title, link: link, virtuals, components)
    }
    
    func saveInDB(_ post: MediumPost) -> Bool {
        guard let data = extractMeaningFulData(post) else { return false }
        let title = data.title
        let link = data.link
        
        //        CachivachePost(title: post.title, subtitle: virtuals.subtitle, emailSnippet: virtuals.emailSnippet, readTime: virtuals.readingTime, publishedDate: virtuals.airDate, tags: virtuals.tags, imageId: virtuals.imageId, imageAspectRatio_HdivW: virtuals.imageAspectRatio, favorited: false, deleted: false, read: false, readProgress: 0, postBody: postsReader.getComponents()
        //        let post = CachivachePost()
        //        post.title = post.title
        //
        
        
        
        let post = CachivachePost()
        post.initRealm()
        
        if let post = PostDataAccess.findByLink(link) {
            
            //   print("Exists in database (\(title)) ..must update")
            if post.publishedDate != data.2.airDate || post.postBody.isEmpty{
                
                
                PostDataAccess.updateValues({_ in
                    post.title = title
                    
                    post.emailSnippet = data.2.emailSnippet
                    post.subtitle = data.2.subtitle
                    post.readTime = data.2.readingTime
                    post.publishedDate = data.2.airDate
                    post.setPostTags(data.2.tags)
                    post.imageId = data.2.imageId
                    post.imageAspectRatio_HdivW = data.2.imageAspectRatio
                    
                    let body: [Components] = {
                        let headers = [
                           Components.metadata(timeLength: post.getReadingTime(), daysElapsed: post.getDaysSincePublished())
                        ]
                        return headers + data.3
                    }()
                    
                    post.setPostBody(body)
                    
                })
            }//else {
            //  print("..cancel update, post already download")
            //  }
            return true
        }
        
        
        post.title = title
        post.link = link
        post.emailSnippet = data.2.emailSnippet
        post.subtitle = data.2.subtitle
        post.readTime = data.2.readingTime
        post.publishedDate = data.2.airDate
        post.setPostTags(data.2.tags)
        post.imageId = data.2.imageId
        post.imageAspectRatio_HdivW = data.2.imageAspectRatio
        
        let body: [Components] = {
            let headers = [
               Components.metadata(timeLength: post.getReadingTime(), daysElapsed: post.getDaysSincePublished())
            ]
            return headers + data.3
        }()
        
        
        post.setPostBody(body)
        
        
        PostDataAccess.persist(post, update: false)
        precacheImagesFor(post: post)
        
        return true
    }
    
    func precacheImagesFor(post: CachivachePost) {
        let images = post.postBody
            .map { (component) -> (id:String, size: ImageSource.ImageSizes)? in
                switch component {
                case let .image(imageId, originalRatio, _,_): return (id: imageId, size: ImageSource.ImageSizes.fullWidth(originalRatio: originalRatio))
                case let .link( _,imageId,_, _,_,originalRatio): return (id: imageId, size: ImageSource.ImageSizes.squareLink(originalRatio: originalRatio))
                default: return nil
                }
            }
            .filter { $0 != nil && !$0!.id.isEmpty }
            .map { $0! }
        
        ImageSource.precacheImages(images) { success in
            PostDataAccess.updateValues({ _ in
                post.allImagesPrecached = success
            })
        }
    }
    
    func reportDoneDownloading() {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "DoneDownloading"), object: nil)
        print("Done Downloading")
    }
    
    func reportStartDownloading() {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "StartDownloading"), object: nil)
        print("Start Downloading")
    }
    
    func previewPost(_ mediumPost: MediumPost) -> FeedNetworkService.PreviewPost {
        return FeedService.previewPost(mediumPost)
    }
    
    fileprivate static let errorValidationPost = { (result: Result<MediumPost,RequestError>) -> Observable<MediumPost> in
        switch result {
        case .success(let post):
            return Observable.just(post)
        case let .failure(error):
            switch error {
            case let .mediumServer(_, _, failedPost):
                ErrorHandler.reportNewError(error)
                //attempRetryfromGoogle
                guard let failedPost = failedPost else { return Observable.just(MediumPost())}
                return MediumPostService.retryFromBackupSource(failedPost)
            default:
                ErrorHandler.reportNewError(error)
                
            }
          return Observable.just(MediumPost())
        }
    }
    
    fileprivate static func retryFromBackupSource(_ failedPost: FeedNetworkService.PreviewPost) -> Observable<MediumPost> {
        if let backupUrl = failedPost.backupDownloadUrl {
            return getMediumPost(backupUrl, forPreviewPost: failedPost)
        }
        return Observable.just(MediumPost())
    }
    
    fileprivate static func getMediumPost(_ url:String, forPreviewPost previewPost: FeedNetworkService.PreviewPost) -> Observable<MediumPost> {
        return NetworkService().getData(Routes.directRoute(url: url), forPreviewPost: previewPost)
            //  .observeOn(ConcurrentDispatchQueueScheduler(globalConcurrentQueueQOS: .Background))
            .observeOn(MainScheduler.instance)
            .flatMapLatest(errorValidationPost)
            .catchError({ (error) -> Observable<MediumPost> in
                return Observable.empty()
            })
    }
}
