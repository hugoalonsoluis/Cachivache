//
//  ImageService.swift
//  Marvel Characters
//
//  Created by Hugo on 5/26/16.
//  Copyright © 2016 halonsoluis. All rights reserved.
//

import Foundation
import Kingfisher
import UIKit

/**
 Prepares for loading images and handle the cache of those already downloaded
 */
struct ImageSource {
    fileprivate static let mockupEnabled : Bool = { return ProcessInfo.processInfo.arguments.contains("MOCKUP_MODE") }()
    
    fileprivate static let placeHolderImage = UIImage(named: "Image_not_found")
    
    enum ImageSizes {
        case fullWidth(originalRatio: Double), squareLink(originalRatio: Double)
        
        func getDownloadImageSize() -> CGSize {
            var width = min(UIScreen.main.bounds.width, UIScreen.main.bounds.height)
            let height : CGFloat!
            switch self {
            case let .fullWidth(originalRatio):
                height = width / CGFloat(originalRatio)
            case let .squareLink(originalRatio):
                width = width / 2
                height = (width) / CGFloat(originalRatio)
            }
            return CGSize(width: width, height: height)
        }
    }
    
    
    static func setupMaxImageDurationInCache(_ time: TimeInterval){
        let cache = KingfisherManager.shared.cache
        
        // Set max disk cache to duration to time
        cache.maxCachePeriodInSecond = time
    }                                    

    
    
    /**
     Download/Load from cache the image and set it at the specified imageView
     
     - parameter imageView:         destination UIImageView
     - parameter uniqueKey:         unique resource location in cache
     - parameter completionHandler: returns the loaded image (from web or cache)
     */
    static func downloadImageAndSetIn(_ imageView: UIImageView, imageId: String, size: ImageSizes, mockupImage: UIImage? = nil, completionHandler: ((Image?)->())? = nil){
        let size = size.getDownloadImageSize()
        let image = Routes.imageUrl(id: imageId, width: Int(size.width), height: Int(size.height))
        
        guard !mockupEnabled || mockupImage != nil else {
            let image = loadMockupImage(image)
            imageView.image = image
            completionHandler?(image)
            return
        }
        
        let path = image.getRoute()
        let resource = URL(string: path)!
        var placeholderImage = getPlaceholderImage() ?? imageView.image
        placeholderImage = mockupImage ?? placeholderImage
        
        
        imageView.kf.setImage(with: resource, placeholder: placeholderImage, options: [.transition(ImageTransition.fade(1))], progressBlock: nil, completionHandler: {(image, _, _ ,_) in
            completionHandler?(image)
        })
    }
    
    static func loadImageAsync(_ imageId: String, size: ImageSizes, mockupImage: UIImage? = nil, completionHandler: ((Image?)->())? = nil){
        let size = size.getDownloadImageSize()
        let image = Routes.imageUrl(id: imageId, width: Int(size.width), height: Int(size.height))
        
        guard !mockupEnabled || mockupImage != nil else {
            let image = loadMockupImage(image)
            completionHandler?(image)
            return
        }
        let path = image.getRoute()
        let resource = URL(string: path)!
       
        KingfisherManager.shared.retrieveImage(with: resource, options: nil, progressBlock: nil) { (image, _, _, _) in
             completionHandler?(image)
        }
    }
    
    static func precacheImages(_ images: [(id:String, size: ImageSizes)], done: @escaping (Bool) ->Void) {
        
        let urls = images.map { (image) -> URL in
            let size = image.size.getDownloadImageSize()
            let route = Routes.imageUrl(id: image.id, width: Int(size.width), height: Int(size.height))
            return URL(string: route.getRoute())!
        }
        var processedImages = 0
        let prefetcher = ImagePrefetcher(urls: urls, progressBlock: { _ in
            processedImages = processedImages.advanced(by: 1)
            print("Processed \(processedImages) of \(urls.count)")
        }) { (skippedResources, failedResources, completedResources) in
            done(failedResources.isEmpty)
            print("Finished processing images = (skipped: \(skippedResources.count), failed: \(failedResources.count), success: \(completedResources.count))")
        }
        prefetcher.start()
        
        
    }
    
    fileprivate static func loadMockupImage(_ imageRoute: Routes) -> UIImage? {
        return UIImage(data: MockupResourceLoader.getMockupData(imageRoute)!)
    }
    
    /**
     Obtains a placeholder Image to show when downloading resource
     
     - returns: a placeholder Image
     */
    static fileprivate func getPlaceholderImage() -> UIImage? {
        //mockupEnabled ? UIImage(data: MockupResource.Image.getMockupData()!) :
        return placeHolderImage
    }
}
