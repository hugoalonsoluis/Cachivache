//
//  APIHandler.swift
//  Marvel Characters
//
//  Created by Hugo Alonso on 5/26/16.
//  Copyright © 2016 halonsoluis. All rights reserved.
//

import Foundation


struct APIHandler {
    
   static func getDefaultParamsAsDict() -> [String:String] {
        return {
            
            var dict = [String:String]()
            
            dict["format"] = "json"
          
            return dict
        }()
        
    }
    
}