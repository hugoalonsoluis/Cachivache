//
//  BreakError.swift
//  Marvel Characters
//
//  Created by Hugo on 5/26/16.
//  Copyright © 2016 halonsoluis. All rights reserved.
//

import Foundation

enum RequestError: Error{
    case mediumServer(String, forUrl: String, using: FeedNetworkService.PreviewPost?)
    case cancelled
    case timeout
    case unknown
    case parser(String)
    case error(String)
    case failedBackupDownload(forUrl: String)
    
    func errorDescription() -> String {
        switch self {
        case let .error(string: message): return message
            
        case let .mediumServer(message, _, _): return message
        case .cancelled: return "Cancelled"
        case .timeout: return "Timeout"
        case .unknown: return "Unknown"
        case let .parser(message): return message
        case let .failedBackupDownload(forUrl): return "Failed backup download for: \(forUrl)"
        }
    }
    
    func printableErrorDescription() -> String {
        switch self {
        case let .error(message): return "An error of type Error with message '\(message)' has being raised"
        case let .parser(message): return "Parser error while processing request made to '\(message)'"
        case let .mediumServer(message, url,_): return "Medium raised an \"\(message)\" for request made to '\(url)'"
            
        default:  return "An error of type '\(self.errorDescription())' has being raised"
        }
    }
}
