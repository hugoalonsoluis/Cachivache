//
//  FeedNetworkService.swift
//  Cachivache
//
//  Created by Hugo Alonso on 6/9/16.
//  Copyright © 2016 Hugo Alonso. All rights reserved.
//

import Foundation
import Result
import RxSwift

class FeedNetworkService {
    
    class PreviewPost {
        var title: String?
        var snippet: String?
        var link: String?
        var imageId: String?
        var mediaResource: String?
        var backupDownloadUrl: String?
        //local metadata
        var new: Bool = false
        var favorited: Bool = false
        var tags: [String] = []
    }
    
       
    fileprivate init() {}
    
    fileprivate  static let errorValidationPost = { (result: Result<MediumPost,RequestError>) -> Observable<MediumPost> in
        switch result {
        case .success(let post):
            return Observable.just(post)
        case .failure(let error):
            ErrorHandler.reportNewError(error)
            return Observable.empty()
        }
    }
    
    fileprivate static let convertToPreviewPost = { (allInOnePost: MediumPost) -> Observable<[PreviewPost]> in
        guard let previewPosts = FeedNetworkService.extractPostsDepictedIn(allInOnePost) else { return Observable.empty() }
        return Observable.just(previewPosts)
    }
    
    static var networkPostPreviewFeed : Observable<[FeedNetworkService.PreviewPost]> {
       return NetworkService().getData(Routes.allPost, forPreviewPost: nil)
      //  .observeOn(ConcurrentDispatchQueueScheduler(globalConcurrentQueueQOS: .Background))
        .observeOn(MainScheduler.instance)
        .flatMapLatest(errorValidationPost)
        .observeOn(ConcurrentDispatchQueueScheduler.init(queue: DispatchQueue.global(qos: DispatchQoS.QoSClass.background)))
        .flatMapLatest(convertToPreviewPost)
        .catchError({ (error) -> Observable<[FeedNetworkService.PreviewPost]> in
            print(error)
            return Observable.empty()
        })
    }
    
    
    fileprivate static func extractPostsDepictedIn(_ allinOnePost: MediumPost) -> [PreviewPost]? {
         guard let postsBox = MediumPostReader(post: allinOnePost).getComponents() else { return nil }
        
        var posts = [PreviewPost]()
        
        for post in postsBox {
            switch post {
            case let .link(href, thumbnailImageId, _, text, mediaResourceId, _):
            
            let newPost = PreviewPost()
                
            let parts = text.components(separatedBy: "\n")
            let postTitle = parts.first
            var postSubtitle = parts.dropFirst()
            
            var first = postSubtitle.first
            if let extractionResult = extractBackupUrl(first) {
                newPost.backupDownloadUrl = extractionResult.url
                //remove code from subtitle construction
                first = extractionResult.snippetWithoutCode
            }
            
            if let first = first, first.contains("Por: ") {
                postSubtitle = postSubtitle.dropFirst()
            }
            
            if let last = postSubtitle.last?.components(separatedBy: "medium.com").first {
                postSubtitle = postSubtitle.dropLast()
                postSubtitle.append(last)
            }
            
            
           
            newPost.title = String(postTitle!)
            newPost.snippet = postSubtitle.joined(separator: "\n")
            
            newPost.link = href.absoluteString
            newPost.imageId = thumbnailImageId
            newPost.mediaResource = mediaResourceId
            
            posts.append(newPost)
                
            default: continue
            }
        }
        
        return posts
    }
    
    fileprivate static func extractBackupUrl(_ string: String?) -> (url:String, snippetWithoutCode :String)? {
        
        guard let start = string?.components(separatedBy: " ").first, start.characters.count >= 28 else { return nil }
        let codeEndRange = start.characters.index(start.characters.startIndex, offsetBy: 28)
        let code = start.substring(to: codeEndRange)
        
        guard code.characters.count == 28 else { return nil }
        
        let snippetWithoutCode = string!.replacingCharacters(in: string!.startIndex..<codeEndRange, with: "")
        return (url: "https://docs.google.com/uc?authuser=0&id=\(code)&export=download", snippetWithoutCode: snippetWithoutCode)
    }
}
