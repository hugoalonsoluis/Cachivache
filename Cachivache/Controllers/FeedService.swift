//
//  FeedService.swift
//  Cachivache
//
//  Created by Hugo Alonso on 6/12/16.
//  Copyright © 2016 Hugo Alonso. All rights reserved.
//

import Foundation
import Result
import RxSwift

class FeedService {
    let disposeBag = DisposeBag()
    
    lazy var networkFeed : Observable<[FeedNetworkService.PreviewPost]> = {
        return FeedNetworkService.networkPostPreviewFeed
    }()
    
    var dbFeed : Observable<[FeedNetworkService.PreviewPost]> {
        return Observable.just(PostsStatus.stored.getAll())
            .flatMapLatest(convertToPreviewPost)
    }
    
    fileprivate let convertToPreviewPost = { (dbposts: [CachivachePost]) -> Observable<[FeedNetworkService.PreviewPost]> in
        return Observable.just(dbposts.map({ (post) -> FeedNetworkService.PreviewPost in
            return FeedService.previewPost(post)
        }))
    }
    
    static func previewPost(_ post: CachivachePost) -> FeedNetworkService.PreviewPost {
        let previewPost = FeedNetworkService.PreviewPost()
        previewPost.title = post.title
        previewPost.imageId = post.imageId
        previewPost.link = post.link
        previewPost.snippet = post.subtitle
        previewPost.new = !post.read
        previewPost.tags = post.postTags
        previewPost.favorited = post.favorited
        
        return previewPost
    }
    
    static func previewPost(_ post: MediumPost) -> FeedNetworkService.PreviewPost {
        //   guard let post = post else { return nil }
        
        let previewPost = FeedNetworkService.PreviewPost()
        previewPost.title = post.title
        previewPost.imageId = post.virtuals?.previewImage?.imageId
        previewPost.link = post.webCanonicalUrl
        previewPost.snippet = post.virtuals?.subtitle
        
        previewPost.tags = (post.virtuals?.tags?
            .filter { $0.name != nil }
            .map { $0.name! }) ?? []
        previewPost.new = true
        previewPost.favorited = false
        
        return previewPost
    }
    
    func searchFor(_ text: String) -> Observable<[FeedNetworkService.PreviewPost]>{
        return Observable.just(PostProvider().searchInAll(text))
            .flatMapLatest(convertToPreviewPost)
    }
    
    //  var newPostCardsVar = Variable<[FeedNetworkService.PreviewPost]>([])
    
    fileprivate var newPostCards : Observable<[FeedNetworkService.PreviewPost]>!
    var allPosts: Observable<[FeedNetworkService.PreviewPost]>!
    
    init() {
        newPostCards = {
            return Observable
                .combineLatest(networkFeed, dbFeed) { (network, local) -> [FeedNetworkService.PreviewPost] in
                    return self.reportNewElements(network, old: local)
            }
        }()
        
        //        newPostCards
        //            .bindTo(newPostCardsVar)
        //            .addDisposableTo(disposeBag)
        //
        /*
         allPosts = {
         return Observable.combineLatest(dbFeed, newPostCards) { (stored, new) -> [FeedNetworkService.PreviewPost] in
         print("\(stored.count) downloaded, \(new.count) pending for download")
         var all = stored
         all.appendContentsOf(new)
         return all
         }
         }()*/
    }
    
    func downloadNew() -> Observable<FeedNetworkService.PreviewPost?> {
        return newPostCards
            .asObservable()
            .flatMapLatest { (newPosts) -> Observable<FeedNetworkService.PreviewPost?> in
                guard let service = MediumPostService(newPosts: newPosts) else { return Observable.just(nil) }
                let downloadObserver = service
                    .downloadedPost
                    .asObservable()
                    .debounce(5, scheduler: MainScheduler.asyncInstance)
                    .filter { $0 != nil }
                    .flatMapLatest({ (post) -> Observable<FeedNetworkService.PreviewPost?> in
                        //  print("Downloaded \(post!.title)");
                        return Observable.just(FeedService.previewPost(post!))
                    })
                service.startDownloading()
                return downloadObserver
        }
    }
    
    func refreshNetworkFeed() {
        networkFeed.subscribe().addDisposableTo(disposeBag)
    }
    
    func refreshDBFeed() {
        dbFeed.subscribe().addDisposableTo(disposeBag)
    }
    
    fileprivate func reportNewElements(_ new : [FeedNetworkService.PreviewPost] , old: [FeedNetworkService.PreviewPost]  ) -> [FeedNetworkService.PreviewPost] {
        return new
            .filter { newElement in !(old.contains { oldElement in newElement.title == oldElement.title }) }
            .map {
                let newPost = $0
                newPost.new = true
                return newPost
        }
    }
}
