//
//  PostsProvider.swift
//  Cachivache
//
//  Created by Hugo Alonso on 5/8/16.
//  Copyright © 2016 Hugo Alonso. All rights reserved.
//

import Foundation

enum PostsStatus {
    case stored, deleted
    
    func getAll() -> [CachivachePost]{
        let postProvider = PostProvider()
        switch self {
        case .stored:
            return postProvider.getPostsToShow()
        case .deleted:
            return postProvider.deletedPosts()
        }
    }
}

struct PostProvider {

    func getPostsToShow()  -> [CachivachePost] {
        return PostDataAccess.getPosts()
    }
    func deletedPosts()  -> [CachivachePost] {
        return PostDataAccess.getDeleted()
    }
    
    func getTags()  -> [String] {
        let tags = getPostsToShow().reduce([String]()) { $0 + $1.postTags }
            .reduce([String:Int]()) {
                var current = $0
                guard let count = $0[$1] else {
                    current[$1] = 1
                    return current
                }
                current[$1] = count + 1
                return current
            }
            .sorted { $0.1 > $1.1 }
            .map { $0.0 }
     //   print(tags)
        return tags
    }
    
    func searchInAll(_ text: String) -> [CachivachePost] {
       let text = text.lowercased()
       return getPostsToShow().filter {
            $0.postTags.joined(separator: " ").lowercased().contains(text)  ||
            $0.title.lowercased().contains(text)                       ||
            $0.subtitle.lowercased().contains(text)
        }
    }
    
    func searchByTag(_ tag:String)  -> [CachivachePost] {
        return getPostsToShow().filter { $0.postTags.joined(separator: " ").contains(tag)}
    }
    func searchByTitle(_ title:String)  -> [CachivachePost] {
        let title = title.lowercased()
        return getPostsToShow().filter { $0.title.lowercased().contains(title)}
    }
    func searchInSubtitle(_ text:String)  -> [CachivachePost] {
        let text = text.lowercased()
        return getPostsToShow().filter { $0.subtitle.lowercased().contains(text)}
    }
    
    static func sortPostsByPublishedDate(_ postsToSort: [CachivachePost]) -> [CachivachePost]{
        return postsToSort.sorted { (postA, postB) -> Bool in
            guard let p1 = postA.publishedDate, let p2 = postB.publishedDate else {
                return false
            }
            if p1.isAfter(p2) {
                return true
            }
            return false
        }
    }
}
